<?php

namespace Database\Seeders;

use App\Models\SiteSetting;
use Illuminate\Database\Seeder;

class SiteSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $model = new SiteSetting();
        $model->name = 'Book Trade';
        $model->email = 'book@gmail.com';
        $model->address = 'Bhaktapur, Nepal';
        $model->phone = '9876543210';
        $model->facebook_link = 'https://www.facebook.com';
        $model->instagram_link = 'https://www.instagram.com';
        $model->linkedin_link = 'https://www.linkedin.com';
        $model->twitter_link = 'https://www.twitter.com';
        $model->iframe = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.7188975539298!2d85.31264031541535!3d27.695081732636876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19ad6cb5d55d%3A0x27982de11b0fb854!2sDasharath%20Stadium!5e0!3m2!1sen!2snp!4v1667384172385!5m2!1sen!2snp';
        $model->description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, excepturi. Distinctio accusantium fugit odit? Fugit ipsam. Sed ac fringilla ex. Nam mauris velit, ac cursus quis, non leo.';
        $model->addMediaFromUrl(asset('backend/images/logo.png'))->preservingOriginal()->toMediaCollection('default');
        $model->save();
        $model->logo = $model->getMedia()[0]->getFullUrl();
    }
}
