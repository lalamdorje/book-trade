<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Education',
                'order' => 1,
                'status' => 1,
            ],
            [
                'name' => 'Fiction',
                'order' => 2,
                'status' => 1,
            ],
            [
                'name' => 'Non-Fiction',
                'order' => 3,
                'status' => 1,
            ]
        ];
        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
