<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Book;


class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            [
                'category_id'=>'1',
                'sub_category_id'=>'1',
                'ternary_category_id'=>'1',
                'customer_id'=>'1',
                'ad_type'=>'Single',
                'title'=>'How to Attract Money',
                'price_type'=>'Fixed',
                'price'=>'1000',
                'item_condition'=>'New',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'0'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'1',
                'ternary_category_id'=>'1',
                'customer_id'=>'1',
                'ad_type'=>'Single',
                'title'=>'The Lean Startup',
                'price_type'=>'Negotiable',
                'price'=>'500',
                'item_condition'=>'Used',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'0'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'1',
                'ternary_category_id'=>'1',
                'customer_id'=>'1',
                'ad_type'=>'Single',
                'title'=>'Art of Business',
                'price_type'=>'Price on call',
                'price'=>'1200',
                'item_condition'=>'Like New',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'0'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'ternary_category_id'=>'5',
                'customer_id'=>'1',
                'ad_type'=>'Single',
                'title'=>'Financial Accounting II',
                'price_type'=>'Fixed',
                'price'=>'450',
                'item_condition'=>'New',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'ternary_category_id'=>'5',
                'customer_id'=>'2',
                'ad_type'=>'Single',
                'title'=>'Micro Economics',
                'price_type'=>'Negotiable',
                'price'=>'800',
                'item_condition'=>'Used',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'ternary_category_id'=>'5',
                'customer_id'=>'2',
                'ad_type'=>'Single',
                'title'=>'Business Accounting',
                'price_type'=>'Price on call',
                'price'=>'400',
                'item_condition'=>'New',
                'book_edition'=>'2022',
                'description'=>'',
                'status'=>'0',
                'is_featured'=>'1'
            ],

        ];
        foreach($books as $book){
            $book = Book::create($book);
            $book->addMediaFromUrl(asset('frontend/img/book/'.$book->id.'.jpg'))->toMediaCollection('images');
        }
    }
}
