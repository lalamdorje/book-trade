<?php

namespace Database\Seeders;

use App\Models\SubCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subCategories = [
            [
                'category_id' => 1,
                'name' => "Master's",
                'status' => 1,
            ],
            [
                'category_id' => 1,
                'name' => "Bachelor's",
                'status' => 1,
            ],
            [
                'category_id' => 1,
                'name' => '+2',
                'status' => 1,
            ],
            [
                'category_id' => 1,
                'name' => 'School',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Adventure stories',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Classics',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Crime',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Fairy tales, fables, and folk tales',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Historical fiction',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Horror',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Humour and satire',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Mystery',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Poetry',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Romance',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Science fiction',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Short stories',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Thrillers',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'War',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Women’s fiction',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Young adult',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Literary fiction',
                'status' => 1,
            ],
            [
                'category_id' => 2,
                'name' => 'Plays',
                'status' => 1,
            ],
            [
                'category_id' => 3,
                'name' => 'Autobiography and memoir',
                'status' => 1,
            ],
            [
                'category_id' => 3,
                'name' => 'Biography',
                'status' => 1,
            ],
            [
                'category_id' => 3,
                'name' => 'Essays',
                'status' => 1,
            ],
            [
                'category_id' => 3,
                'name' => 'Non-fiction novel',
                'status' => 1,
            ],
            [
                'category_id' => 3,
                'name' => 'Self-help',
                'status' => 1,
            ],
        ];
        foreach ($subCategories as $subCategory) {
            SubCategory::create($subCategory);
        }
    }
}
