<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TernaryCategory;


class TernaryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ternaryCategories=[
            [
                'category_id'=>'1',
                'sub_category_id'=>'1',
                'name'=>'MBA',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'Engineering',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'Medical',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'IT',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'BBA',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'BSC',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'BHM',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'BA',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'BBS',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'2',
                'name'=>'Agriculture & Forestry',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'3',
                'name'=>'Management',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'3',
                'name'=>'Science',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'3',
                'name'=>'Law',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'4',
                'name'=>'Grade 8',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'4',
                'name'=>'Grade 9',
                'status'=>'1'
            ],
            [
                'category_id'=>'1',
                'sub_category_id'=>'4',
                'name'=>'Grade 10',
                'status'=>'1'
            ],

        ];
        foreach($ternaryCategories as $ternaryCategory){
            TernaryCategory::create($ternaryCategory);
        }
    }
}
