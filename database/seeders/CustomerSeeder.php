<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [
                'name' => 'Saroj Poudel',
                'email' => 'saroj@email.com',
                'password' => bcrypt('password'),
                'phone' => 9845121010,
                'address' => 'Kalanki',
            ],
            [
                'name' => 'Raju Poudel',
                'email' => 'raju@email.com',
                'password' => bcrypt('password'),
                'phone' => 9854121478,
                'address' => 'Chitwan',
            ],
            [
                'name' => 'Jeebit Maharjan',
                'email' => 'jeebit@email.com',
                'password' => bcrypt('password'),
                'phone' => 9854578965,
                'address' => 'Lalitpur',
            ],
        ];
        foreach ($customers as $customer) {
            Customer::create($customer);
        }
    }
}
