<?php
if(!function_exists('show_price_format')){
    function show_price_format($price, $currency=null){
        if(empty($currency)){
            $currency = get_currency();
        }
        return $currency.'. '.$price.'/-';
    }
}
if(!function_exists('get_currency')){
    function get_currency(){
        return config('app.currency','RS');
    }
}
