<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index(){
        $contacts=Contact::latest()->get();
        return view('backend.contact', compact('contacts'));
    }
   public function destroy(Contact $contact){
        $contact->delete();
        return redirect()->route('contact.index')->withMsg('Message sucessfully deleted');
   }
}
