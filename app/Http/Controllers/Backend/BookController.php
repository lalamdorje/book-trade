<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BookRequest;
use App\Models\Book;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TernaryCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('media')->latest()->orderBy('is_featured', 'desc')->get();
        return view('backend.book.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::isActive()->latest()->get();
        $subCategories = SubCategory::isActive()->latest()->get();
        $ternaryCategories = TernaryCategory::isActive()->latest()->get();
        return view('backend.book.create', compact('categories', 'subCategories', 'ternaryCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $request['customer_id'] = Auth::guard('customer')->user()->id ?? Auth::user()->id;
        $book = Book::create($request->all());
        foreach ($request->input('images', []) as $file) {
            $book->addMedia(storage_path('frontend/img/book/' . $file))->toMediaCollection('images');
        }
        return redirect()->route('book.index')->withMsg('Book Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $categories = Category::isActive()->latest()->get();
        $subCategories = SubCategory::isActive()->latest()->get();
        $ternaryCategories = TernaryCategory::isActive()->latest()->get();
        return view('backend.book.create', compact('book', 'categories', 'subCategories', 'ternaryCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $request['customer_id'] = Auth::guard('customer')->user()->id ?? Auth::user()->id;
        $book->update($request->all());
    if  (count($book->images) > 0) {
        foreach ($book->images as $media) {
            if (!in_array($media->file_name, $request->input('images', []))) {
                $media->delete();
            }
        }
    }
    $media = $book->images->pluck('file_name')->toArray();

    foreach ($request->input('images', []) as $file) {
        if (count($media) === 0 || !in_array($file, $media)) {
            $book->addMedia(storage_path('frontend/img/book/' . $file))->toMediaCollection('images');
        }
    }

        return redirect()->route('book.index')->withMsg('Book Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        if ($book->hasMedia('images')){
            $book->clearMediaCollection('images');
        }
        $book->delete();
        return redirect()->route('book.index')->withMsg('Book deleted sucessfully');
    }

    public function getTernaryCategories($id){
        $subCategories = TernaryCategory::where('sub_category_id', $id)->get();
        return response(json_encode($subCategories));
    }
    public function storeMedia(Request $request)
    {
    $path = storage_path('frontend/img/book');

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    $file = $request->file('file');

    $name = uniqid() . '_' . trim($file->getClientOriginalName());

    $file->move($path, $name);

    return response()->json([
        'name'          => $name,
        'original_name' => $file->getClientOriginalName(),
    ]);
}
}
