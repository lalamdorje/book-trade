<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\TernaryCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TernaryCategory;
use Illuminate\Http\Request;

class TernaryCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ternaryCategories = TernaryCategory::latest()->get();
        return view('backend.ternary_category.index', compact('ternaryCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::isActive()->get();
        $subCategories = SubCategory::isActive()->get();
        return view('backend.ternary_category.create', compact('categories', 'subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TernaryCategoryRequest $request)
    {
        TernaryCategory::create($request->all());
        return redirect()->route('ternary_category.index')->withMsg('Ternary Category Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TernaryCategory $ternaryCategory)
    {
        $categories = Category::isActive()->get();
        $subCategories = SubCategory::isActive()->get();
        return view('backend.ternary_category.create', compact('categories', 'subCategories', 'ternaryCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TernaryCategoryRequest $request, TernaryCategory $ternaryCategory)
    {
        $ternaryCategory->update($request->all());
        return redirect()->route('ternary_category.index')->withMsg('Ternary Category Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TernaryCategory $ternaryCategory)
    {
        $ternaryCategory->delete();
        return redirect()->route('ternary_category.index')->withMsg('Ternary Category Deleted Successfully.');
    }

    public function getSubCategories($id){
        $subcategories = SubCategory::where('category_id', $id)->get();
        return response(json_encode($subcategories));
    }
}
