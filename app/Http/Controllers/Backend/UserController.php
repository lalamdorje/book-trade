<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function updateProfile(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        return redirect()->back()->with('update-success', 'Your Profile has been successfully updated.');
    }

    public function changePassword(Request $request){
        $this->validate($request, [
            'old_password'=> 'required',
            'new_password' =>'required|min:8|different:old_password',
            'confirm_password' => 'required|min:8|same:new_password',
        ]);
        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;
        $confirmPassword = $request->confirm_password;
        if(Hash::check($oldPassword, auth()->user()->password)){
            if($newPassword == $confirmPassword){
                $user = User::find(auth()->user()->id);
                $user->update(['password' => Hash::make($newPassword)]);
                return redirect()->back()->with('success','Your password has been updated successfully .');
            }else{
                return redirect()->back()->withErrors(['confirm_password' => "Your new password and confirm password didnot matched."]);
            }
        }else{
            // dd('sorry');
            return redirect()->back()->withErrors(['old_password'=>'Your Old Password is invalid']);
        }
    }
}
