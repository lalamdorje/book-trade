<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CUSTOMER_HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    public function showLoginForm()
    {
        return view('customer.login');
    }

    public function handleProviderCallback($driver)
    {
        // dd(url()->previous());
        try {
            $user = Socialite::driver($driver)->user();
            $customer = Customer::updateOrCreate([
                'email' => $user->email,
            ],[
                'name' => $user->name,
                'password' => bcrypt('123456dummy'),
                'phone' => '9812345678',
                'status' => 1,
                customerLoginServiceColumn[$driver] => $user->id
            ]);

            $customer->addMediaFromUrl($user->avatar)->toMediaCollection();
            $this->guard()->login($customer);
            return redirect()->intended($this->redirectPath())->with(['success' => 'Login Successful']);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect('customer/login')->withMsg($e->getMessage());
        }
    }

    public function guard()
    {
        return Auth::guard('customer');
    }

    public function logout(Request $request)
    {
        Auth::guard('customer')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('customer.login');
    }
}
