<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\CustomerRequest;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        if (Auth::guard('customer')->check()){
            $customer = auth()->guard('customer')->user();
            $books = Book::where('customer_id', Auth::guard('customer')->user()->id)->get();
            $allBooks = Book::where('customer_id', Auth::guard('customer')->user()->id)->where('status', 0)->get();
            $soldBooks = Book::where('customer_id', Auth::guard('customer')->user()->id)->where('status', 1)->get();
            return view('customer.profile', compact('customer', 'allBooks', 'soldBooks', 'books'));
        }
        else{
            return redirect()->route('customer.login');
        }
    }

    public function updateProfile(CustomerRequest $request){
        $customer = Auth::guard('customer')->user();
        $customer->update($request->all());
        return redirect()->back()->withMsg('Profile Updated Successfully.');
    }
}
