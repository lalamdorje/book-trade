<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TernaryCategory;
use Illuminate\Http\Request;
use function view;

class BookController extends Controller
{
    public function bookList(){
        $books = Book::orderBy('is_featured', 'desc')->isNotSold()->latest()->simplePaginate(20);
        $categories = Category::isActive()->get();
        $featuredBooks = Book::where('is_featured', 1)->get();
        return view('frontend.book.list', compact('books', 'categories', 'featuredBooks'));
    }

    public function categoryList(Category $category){
        $categories = Category::isActive()->get();
        $featuredBooks = Book::where('is_featured', 1)->get();
        $books = Book::where('category_id', $category->id)->orderBy('is_featured', 'desc')->isNotSold()->latest()->simplePaginate(20);
        return view('frontend.book.list', compact('books', 'categories', 'featuredBooks', 'category'));
    }

    public function bookDetail($id){
        $book=Book::find($id);
        $relatedBooks=Book::where('ternary_category_id',$book->id)->whereNotIn('id',[$book->id])->latest()->get();
        return view('frontend.book.detail',compact('book','relatedBooks'));
    }
    public function subCategoryList(SubCategory $subCategory){
        $categories = Category::isActive()->get();
        $featuredBooks = Book::where('is_featured', 1)->get();
        $books = Book::where('sub_category_id', $subCategory->id)->orderBy('is_featured', 'desc')->isNotSold()->latest()->simplePaginate(20);
        return view('frontend.book.list', compact('books', 'categories', 'featuredBooks', 'subCategory'));
    }
    public function ternaryCategoryList(TernaryCategory $ternaryCategory){
        $categories = Category::isActive()->get();
        $featuredBooks = Book::where('is_featured', 1)->get();
        $books = Book::where('ternary_category_id', $ternaryCategory->id)->orderBy('is_featured', 'desc')->isNotSold()->latest()->simplePaginate(20);
        return view('frontend.book.list', compact('books', 'categories', 'featuredBooks', 'ternaryCategory'));
    }

    public function search(Request $request){
        $categories = Category::isActive()->get();
        $featuredBooks = Book::where('is_featured', 1)->get();
        if ($request->search){
            $books = Book::where('title', 'LIKE', '%'.$request->search.'%')->isNotSold()->latest()->simplePaginate(20);
        }else{
            $books = Book::where('category_id', $request->category_id)
                ->where('title', 'LIKE', '%'.$request->title.'%')
                ->whereHas('customer', function ($query) use($request){
                $query->where('address', 'LIKE', '%'.$request->address.'%');
            })->isNotSold()->latest()->simplePaginate(20);
        }
        $searchedCategory = Category::find($request->category_id);
        return view('frontend.book.list', compact('books', 'categories', 'featuredBooks', 'searchedCategory'));
    }
}
