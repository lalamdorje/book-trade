<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BookRequest;
use App\Models\Book;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TernaryCategory;
use Illuminate\Support\Facades\Auth;
use function view;

class PostAdvertisementController extends Controller
{
    public function index(){
        if(Auth::guard('customer')->check()){
            $categories = Category::isActive()->latest()->get();
            $subCategories = SubCategory::isActive()->latest()->get();
            $ternaryCategories = TernaryCategory::isActive()->latest()->get();
            return view('customer.post_ad', compact('categories', 'subCategories', 'ternaryCategories'));
        }else{
            return redirect()->route('customer.login');
        }
    }

    public function store(BookRequest $request)
    {
        $request['customer_id'] = Auth::guard('customer')->user()->id ?? Auth::user()->id;
        $book = Book::create($request->all());
        foreach ($request->input('images', []) as $file) {
            $book->addMedia(storage_path('frontend/img/book/' . $file))->toMediaCollection('images');
        }
        return redirect()->route('customer.home')->withMsg('Book Uploaded Successfully.');
    }
    public function edit(Book $book)
    {
        $categories = Category::isActive()->latest()->get();
        $subCategories = SubCategory::isActive()->latest()->get();
        $ternaryCategories = TernaryCategory::isActive()->latest()->get();
        return view('customer.post_ad', compact('book', 'categories', 'subCategories', 'ternaryCategories'));
    }

    public function update(BookRequest $request, Book $book)
    {
        $request['customer_id'] = Auth::guard('customer')->user()->id ?? Auth::user()->id;
        $book->update($request->all());
        if  (count($book->images) > 0) {
            foreach ($book->images as $media) {
                if (!in_array($media->file_name, $request->input('images', []))) {
                    $media->delete();
                }
            }
        }
        $media = $book->images->pluck('file_name')->toArray();

        foreach ($request->input('images', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $book->addMedia(storage_path('frontend/img/book/' . $file))->toMediaCollection('images');
            }
        }

        return redirect()->route('customer.home')->withMsg('Book Updated Successfully.');
    }

    public function destroy(Book $book)
    {
        if ($book->hasMedia('images')){
            $book->clearMediaCollection('images');
        }
        $book->delete();
        return redirect()->back()->withMsg('Book deleted successfully.');
    }

    public function soldBook($id){
        $book = Book::find($id);
        $book->status = 1;
        $book->save();
        return response(json_encode($book));
    }

}
