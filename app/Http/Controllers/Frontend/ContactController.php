<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Http\Requests\Backend\ContactRequest;
class ContactController extends Controller
{
    public function contact(){
        $siteSettings=SiteSetting::first();
        return view('frontend.contact',compact('siteSettings'));
    }
    public function store(ContactRequest $request){
        Contact::create($request->all());
        return redirect()->back()->withMsg('Thank your for your message');
    }
}
