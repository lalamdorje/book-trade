<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use App\Models\Category;

class HomeController extends Controller
{
    public function index(){
        $categories = Category::isActive()->get();
        $siteSetting = SiteSetting::first();
        return view('frontend.index', compact('siteSetting', 'categories'));
    }
}
