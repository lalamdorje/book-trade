<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules =  [
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'ternary_category_id' => 'nullable',
            'ad_type' => 'required',
            'title' => 'required',
            'price_type' => 'required',
            'price' => 'required',
            'item_condition' => 'required',
            'book_edition' => 'required',
            'description' => 'nullable',
            
        ];

        // if ($this->method() == 'PATCH' || $this->method() == 'PUT')
        // {
        //     $rules['images'] = 'nullable|file|image:png,jpeg,jpg,gif';
        // }

        return $rules;

      
    }
}
