<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class TernaryCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'name' => 'required|unique:ternary_categories',
        ];
        if ($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['name'] = 'required';
        }
        return $rules;
    }
}
