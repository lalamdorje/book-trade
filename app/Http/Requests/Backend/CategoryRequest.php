<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:categories',
            'order' => 'required|unique:categories'
        ];
        if ($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['name'] = 'required';
            $rules['order'] = 'required';
        }
        return $rules;
    }
}
