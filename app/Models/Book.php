<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Book extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    protected $fillable = ['category_id', 'sub_category_id', 'ternary_category_id', 'customer_id','ad_type', 'title', 'price_type', 'price', 'item_condition', 'book_edition', 'description', 'status', 'is_featured'];
    protected $appends = ['images'];

   public function getImagesAttribute(){
       return $this->hasMedia('images') ? $this->getMedia('images') :'Images' ;
   }
    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function sub_category(){
        return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
    }
    public function ternary_category(){
        return $this->belongsTo(TernaryCategory::class, 'ternary_category_id', 'id');
    }
    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
    public function scopeIsSold($query){
        return $query->where('status', 1);
    }
    public function scopeIsNotSold($query){
        return $query->where('status', 0);
    }
}
