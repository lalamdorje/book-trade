<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'order', 'status'];

    public function scopeIsActive($query){
        return $query->where('status', 1);
    }

    public function sub_categories(){
        return $this->hasMany(SubCategory::class);
    }

    public function ternary_categories(){
        return $this->hasMany(TernaryCategory::class);
    }
}
