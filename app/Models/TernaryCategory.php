<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TernaryCategory extends Model
{
    use HasFactory;
    protected $fillable = ['category_id', 'sub_category_id', 'name', 'status'];

    public function scopeIsActive($query){
        return $query->where('status', 1);
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function sub_category(){
        return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
    }
}
