<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $fillable = ['category_id', 'name', 'status'];

    public function scopeIsActive($query){
        return $query->where('status', 1);
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function ternary_categories(){
        return $this->hasMany(TernaryCategory::class);
    }
}
