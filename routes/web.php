<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'App\Http\Controllers\Backend', 'middleware' => 'auth'],function(){
    /* Dashboard */
//    Route::get('/', function () {
//        return view('backend.dashboard');
//    });
    Route::get('dashboard', function () {
        return view('backend.dashboard');
    })->name('dashboard');
    Route::get('profile', function () {
        return view('auth.profile');
    })->name('admin.profile');
    Route::post('update-profile','UserController@updateProfile')->name('auth.update-profile');
    Route::post('change-password', 'UserController@changePassword')->name('auth.change-password');


    Route::resource('site-setting', 'SiteSettingController');
    Route::resource('category', 'CategoryController');
    Route::resource('sub_category', 'SubCategoryController');
    Route::resource('ternary_category', 'TernaryCategoryController');
    Route::resource('book', 'BookController');
    Route::resource('contact', 'ContactController');
    Route::get('/getSubCategories/{id}', [\App\Http\Controllers\Backend\TernaryCategoryController::class, 'getSubCategories']);
    Route::get('/getTernaryCategories/{id}', [\App\Http\Controllers\Backend\BookController::class, 'getTernaryCategories']);
    Route::post('projects/media', [\App\Http\Controllers\Backend\BookController::class, 'storeMedia'])->name('books.storeMedia');

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

