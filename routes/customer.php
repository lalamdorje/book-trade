<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Customer\Auth\RegisterController;
use App\Http\Controllers\Customer\Auth\LoginController;
use App\Http\Controllers\Customer\HomeController;

/*
|--------------------------------------------------------------------------
| Customer Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'customer'], function () {
    /*Customer Register*/
    Route::get('register', [RegisterController::class, 'showRegisterForm'])->name('customer.register')->middleware('guest:customer');
    Route::post('register', [RegisterController::class, 'register'])->name('customer.register.store')->middleware('guest:customer');
    /*Customer Login*/
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('customer.login')->middleware('guest:customer');
    Route::post('login', [LoginController::class, 'login'])->name('customer.login.store')->middleware('guest:customer');
    /*Customer Logout*/
    Route::post('logout', [LoginController::class, 'logout'])->name('customer.logout');

    Route::group(['middleware' => 'customer'], function () {
        Route::get('/home', [HomeController::class, 'index'])->name('customer.home');
        Route::post('/home/update-profile', [HomeController::class, 'updateProfile'])->name('customer.profile.update');
    });
});

Route::get('/login/{driver}', [LoginController::class, 'redirectToProvider'])->name('login.via.driver');
Route::get('/login/{driver}/callback', [LoginController::class, 'handleProviderCallback']);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
