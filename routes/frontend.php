<?php

use App\Http\Controllers\Frontend\BookController;
use App\Http\Controllers\Frontend\PostAdvertisementController;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('frontend.home');
Route::get('/contact', 'ContactController@contact')->name('frontend.contact');
Route::post('/contact/store', 'ContactController@store')->name('frontend.contact.store');

Route::get('/post-ad', [PostAdvertisementController::class, 'index'])->name('frontend.post_ad');
Route::post('/post-ad/book/store', [PostAdvertisementController::class, 'store'])->name('frontend.post_ad.store');
Route::get('/post-ad/book/{book}/edit', [PostAdvertisementController::class, 'edit'])->name('frontend.post_ad.edit');
Route::patch('/post-ad/book/{book}/update', [PostAdvertisementController::class, 'update'])->name('frontend.post_ad.update');
Route::get('/book/sold/{id}', [PostAdvertisementController::class, 'soldBook'])->name('frontend.book.sold');
Route::delete('/book/{book}/delete', [PostAdvertisementController::class, 'destroy'])->name('frontend.book.delete');
Route::get('/getSubCategories/{id}', [\App\Http\Controllers\Backend\TernaryCategoryController::class, 'getSubCategories']);
Route::get('/getTernaryCategories/{id}', [\App\Http\Controllers\Backend\BookController::class, 'getTernaryCategories']);
Route::get('/books/list', [BookController::class, 'bookList'])->name('frontend.book.list');
Route::get('/books/category/{category}/list', [BookController::class, 'categoryList'])->name('frontend.book.category.list');
Route::get('/book/{id}/detail', [BookController::class, 'bookDetail'])->name('frontend.book.detail');
Route::get('/books/sub-category/{sub_category}/list', [BookController::class, 'subCategoryList'])->name('frontend.book.subcategory.list');
Route::get('/books/ternary-category/{ternary_category}/list', [BookController::class, 'ternaryCategoryList'])->name('frontend.book.ternary_category.list');
Route::get('/books/search', [BookController::class, 'search'])->name('frontend.book.search');
Route::post('projects/media', [\App\Http\Controllers\Backend\BookController::class, 'storeMedia'])->name('books.storeMedia');


