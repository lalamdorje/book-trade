@extends('backend.layouts.app')
@section('content')
    <!-- News Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Ternary Category Section</span>
            <h3 class="page-title">Add New Ternary Category</h3>
        </div>
    </div>
    <!-- End Ternary-Category Header -->
    <form class="add-new-post" action="{{isset($ternaryCategory)?route('ternary_category.update', $ternaryCategory):route('ternary_category.store')}}" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        @csrf
                        @if(isset($ternaryCategory))
                            @method('PATCH')
                        @endif
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Category<span class="required">*</span> </label>
                            <select name="category_id" id="category_id" class="form-control form-control-lg mb-3">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @isset($ternaryCategory)@if($category->id == $ternaryCategory->category_id) selected @endif @endisset>{{$category->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('category_id'))
                                <div class="text text-danger">
                                    {{$errors->first('category_id')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Sub-Category<span class="required">*</span> </label>
                            <select name="sub_category_id" id="sub_category_id" class="form-control form-control-lg mb-3">
                                <option value="">Select Sub-Category</option>
                                @foreach ($categories as $category )
                                    <option value="{{$category->id}}" @isset($ternaryCategory)@if($subCategory->id == $ternaryCategory->sub_category_id) selected @endif @endisset>$subCategory->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->first('sub_category_id'))
                                <div class="text text-danger">
                                    {{$errors->first('sub_category_id')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Name <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="name" type="text" name="name" value="{{old('name')?old('name'):(isset($ternaryCategory)?$ternaryCategory->name:'')}}" placeholder="Your Ternary Category Name">
                            @if($errors->first('name'))
                                <div class="text text-danger">
                                    {{$errors->first('name')}}
                                </div>
                            @endif
                        </div>Book
                    </div>
                </div>
                <!-- / Add New Post Form -->
            </div>
            <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Actions</h6>
                    </div>
                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                 <span class="d-flex">
                                    <div class="custom-control">
                                        <input type="hidden" name="status" value="0">
                                        <input type="checkbox" class="custom-control-input" id="customSwitch" name="status"
                                               value="1" {{ (isset($ternaryCategory) && $ternaryCategory->status == 1) || old('status') ? 'checked' : ''}}>
                                        <label class="custom-control-label" for="customSwitch">Is Active</label>
                                    </div>
                                </span>
                            </li>
                            <li class="list-group-item d-flex px-3">
                                <button type="submit" id="draft" class="btn btn-sm btn-outline-accent" name="status" value="draft">
                                    <i class="material-icons">save</i> Save Draft
                                </button>
                                <button type="submit" class="btn btn-sm btn-accent ml-auto">
                                    <i class="material-icons">file_copy</i> Publish
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / Post Overview -->
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#category_id').on('change', function () {
                let id = $(this).val();
                $('#sub_category_id').empty();
                $('#sub_category_id').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                    type: 'GET',
                    url: '/admin/getSubCategories/' + id,
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#sub_category_id').empty();
                        $('#sub_category_id').append(`<option value="" disabled selected>Select Sub-Category</option>`);
                        response.forEach(element => {
                            $('#sub_category_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                        });
                    }
                });
            });
        });
    </script>
@endsection
