@extends('backend.layouts.app')
@section('styles')
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" referrerpolicy="no-referrer" />--}}
    <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>

    <style>
        .dropzone .dz-preview .dz-image img {
            display: block;
            height: 100%;
            width: 100%;
            object-fit: contain;
        }
    </style>
@endsection
@section('content')
    <!-- News Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Book Section</span>
            <h3 class="page-title">Add New Book</h3>
        </div>
    </div>
    <!-- End Ternary-Category Header -->
    <form class="add-new-post" action="{{isset($book)?route('book.update', $book):route('book.store')}}" method="post"
          enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        @csrf
                        @if(isset($book))
                            @method('PATCH')
                        @endif
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Category<span class="required">*</span> </label>
                            <select name="category_id" id="category_id" class="form-control form-control-lg mb-3">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}"
                                            @isset($book)@if($category->id == $book->category_id) selected @endif @endisset>{{$category->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('category_id'))
                                <div class="text text-danger">
                                    {{$errors->first('category_id')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Sub-Category<span class="required">*</span> </label>
                            <select name="sub_category_id" id="sub_category_id"
                                    class="form-control form-control-lg mb-3">
                                <option value="">Select Sub-Category</option>
                                @foreach($subCategories as $subCategory)
                                    <option value="{{$subCategory->id}}"
                                            @isset($book)@if($subCategory->id == $book->sub_category_id) selected @endif @endisset>{{$subCategory->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('sub_category_id'))
                                <div class="text text-danger">
                                    {{$errors->first('sub_category_id')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3" id="ternaryCategory">
                            <label>Ternary Category</label>
                            <select name="ternary_category_id" id="ternary_category_id"
                                    class="form-control form-control-lg mb-3">
                                <option value="">Select Ternary-Category</option>
                                @foreach($ternaryCategories as $ternaryCategory)
                                    <option value="{{$ternaryCategory->id}}"
                                            @isset($book)@if($ternaryCategory->id == $book->ternary_category_id) selected @endif @endisset>{{$ternaryCategory->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('ternary_category_id'))
                                <div class="text text-danger">
                                    {{$errors->first('ternary_category_id')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Advertisement Type</label>
                            <select name="ad_type" id="ad_type" class="form-control form-control-lg mb-3">
                                <option value="">Select Advertisement Type</option>
                                <option value="Single"
                                        @isset($book)@if($book->ad_type == "Single") selected @endif @endisset>Single
                                </option>
                                <option value="Bulk"
                                        @isset($book)@if($book->ad_type == "Bulk") selected @endif @endisset>Bulk
                                </option>
                            </select>
                            @if($errors->first('ad_type'))
                                <div class="text text-danger">
                                    {{$errors->first('ad_type')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Title <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="title" type="text" name="title"
                                   value="{{old('title')?old('title'):(isset($book)?$book->title:'')}}"
                                   placeholder="Your Book Title">
                            @if($errors->first('title'))
                                <div class="text text-danger">
                                    {{$errors->first('title')}}
                                </div>
                            @endif
                        </div>
                    <!-- @isset($book)
                            @php
                                $collectionName = \Illuminate\Support\Str::slug($book->title);
                            @endphp
                        @endisset -->
                        {{--                        {{dd($book->getMedia(\Illuminate\Support\Str::slug($book->title))[0]->getFullUrl())}}--}}
                        {{--                        <div class="col-md-12 col-sm-12 p-0 pl-3">--}}
                        {{--                            <label>Images<span class="required">*</span></label>--}}
                        {{--                            <input name="image[]" type="file" class="dropify" data-height="200"--}}
                        {{--                                   data-default-file="{{isset($book) ? $book->getMedia($collectionName)[0]->getFullUrl() : ''}}" accept="image/*" multiple/>--}}
                        {{--                            @isset($book)--}}
                        {{--                                @foreach($book->getMedia($collectionName) as $key => $image)--}}
                        {{--                                    @if($key != 0)--}}
                        {{--                                    <img src="{{$image->getFullUrl()}}" alt="" height="100px"; width="100px">--}}
                        {{--                                    @endif--}}
                        {{--                                @endforeach--}}
                        {{--                            @endisset--}}
                        {{--                            @if($errors->first('image'))--}}
                        {{--                                <div class="text text-danger">--}}
                        {{--                                    * {{$errors->first('image')}}--}}
                        {{--                                </div>--}}
                        {{--                            @endif--}}
                        {{--                        </div>--}}

                        <div id="document-dropzone" class="needsclick dropzone"></div>

                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Price Type</label>
                            <select name="price_type" id="price_type" class="form-control form-control-lg mb-3">
                                <option value="">Select Price Type</option>
                                <option value="Fixed"
                                        @isset($book)@if($book->price_type == "Fixed") selected @endif @endisset>Fixed
                                </option>
                                <option value="Negotiable"
                                        @isset($book)@if($book->price_type == "Negotiable") selected @endif @endisset>
                                    Negotiable
                                </option>
                                <option value="Price on call"
                                        @isset($book)@if($book->price_type == "Price on call") selected @endif @endisset>
                                    Price on call
                                </option>
                                {{--                                <option value="Free" @isset($book)@if($book->price_type == "Free") selected @endif @endisset>Free</option>--}}
                            </select>
                            @if($errors->first('price_type'))
                                <div class="text text-danger">
                                    {{$errors->first('price_type')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Price <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="price" type="number" name="price"
                                   value="{{old('price')?old('price'):(isset($book)?$book->price:'')}}"
                                   placeholder="Your Book Price">
                            @if($errors->first('price'))
                                <div class="text text-danger">
                                    {{$errors->first('price')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Item Condition</label>
                            <select name="item_condition" id="item_condition" class="form-control form-control-lg mb-3">
                                <option value="">Select Item Condition</option>
                                <option value="New"
                                        @isset($book)@if($book->item_condition == "New") selected @endif @endisset>New
                                </option>
                                <option value="Used"
                                        @isset($book)@if($book->item_condition == "Used") selected @endif @endisset>Used
                                </option>
                                <option value="Like New"
                                        @isset($book)@if($book->item_condition == "Like New") selected @endif @endisset>
                                    Like New
                                </option>
                            </select>
                            @if($errors->first('item_condition'))
                                <div class="text text-danger">
                                    {{$errors->first('item_condition')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Book Edition <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="book_edition" type="text"
                                   name="book_edition"
                                   value="{{old('book_edition')?old('book_edition'):(isset($book)?$book->book_edition:'')}}"
                                   placeholder="Your Book Edition">
                            @if($errors->first('book_edition'))
                                <div class="text text-danger">
                                    {{$errors->first('book_edition')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Description</label>
                            <textarea id="mytextarea" name="description">{!! isset($book)?$book->description:(old('description') ?? '') !!}</textarea>
                            @if($errors->first('description'))
                                <div class="text text-danger">
                                    *{{$errors->first('description')}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- / Add New Post Form -->
            </div>
            <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Actions</h6>
                    </div>
                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
                            {{--                            <li class="list-group-item p-3">--}}
                            {{--                                 <span class="d-flex">--}}
                            {{--                                    <div class="custom-control">--}}
                            {{--                                        <input type="hidden" name="status" value="0">--}}
                            {{--                                        <input type="checkbox" class="custom-control-input" id="customSwitch" name="status"--}}
                            {{--                                               value="1" {{ (isset($book) && $book->status == 1) || old('status') ? 'checked' : ''}}>--}}
                            {{--                                        <label class="custom-control-label" for="customSwitch">Status</label>--}}
                            {{--                                    </div>--}}
                            {{--                                </span>--}}
                            {{--                            </li>--}}
                            <li class="list-group-item p-3">
                                 <span class="d-flex">
                                    <div class="custom-control">
                                        <input type="hidden" name="is_featured" value="0">
                                        <input type="checkbox" class="custom-control-input" id="customSwitch"
                                               name="is_featured"
                                               value="1" {{ (isset($book) && $book->is_featured == 1) || old('is_featured') ? 'checked' : ''}}>
                                        <label class="custom-control-label" for="customSwitch">Is Featured</label>
                                    </div>
                                </span>
                            </li>
                            <li class="list-group-item d-flex px-3">
                                <button type="submit" id="draft" class="btn btn-sm btn-outline-accent" name="status"
                                        value="draft">
                                    <i class="material-icons">save</i> Save Draft
                                </button>
                                <button type="submit" class="btn btn-sm btn-accent ml-auto">
                                    <i class="material-icons">file_copy</i> Publish
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / Post Overview -->
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js' type='text/javascript'></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
    {{--    <script>--}}
    {{--        $('.dropify').dropify();--}}
    {{--    </script>--}}
    <script>
        $(document).ready(function () {
            $('#category_id').on('change', function () {
                let id = $(this).val();
                let category = $("#category_id option:selected").text();
                console.log(category)
                if (category == "Fiction" || category == "Non-Fiction") {
                    $('#ternaryCategory').removeClass('d-block').addClass('d-none');
                } else {
                    $('#ternaryCategory').removeClass('d-none').addClass('d-block');
                }
                $('#sub_category_id').empty();
                $('#sub_category_id').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                    type: 'GET',
                    url: '/admin/getSubCategories/' + id,
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#sub_category_id').empty();
                        $('#sub_category_id').append(`<option value="" disabled selected>Select Sub-Category</option>`);
                        response.forEach(element => {
                            $('#sub_category_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                        });
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#sub_category_id').on('change', function () {
                let id = $(this).val();
                $('#ternary_category_id').empty();
                $('#ternary_category_id').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                    type: 'GET',
                    url: '/admin/getTernaryCategories/' + id,
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#ternary_category_id').empty();
                        $('#ternary_category_id').append(`<option value="" disabled selected>Select Ternary-Category</option>`);
                        response.forEach(element => {
                            $('#ternary_category_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                        });
                    }
                });
            });
        });
    </script>
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('books.storeMedia') }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                console.log(file);
                $('form').append('<input type="hidden" name="images[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('form').find('input[name="images[]"][value="' + name + '"]').remove()
            },
            init: function () {
                @if(isset($book) && $book->images)
                var files =
                    {!! json_encode($book->images) !!}
                    for(var i in files)
                {
                    var file = files[i]
                    console.log(file);
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="images[]" value="' + file.file_name + '">')
                    $('.dz-image img').attr('src', file.original_url)
                }
                @endif
            }
        }
    </script>
@endsection
