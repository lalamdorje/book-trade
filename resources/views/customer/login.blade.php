@extends('frontend.layouts.app')
@section('content')
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('frontend.home')}}"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Customer Login Page</li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="login-cart-section">
        <div class="container px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-5 pt-5 mb-5 border-line"><img src="{{asset('frontend/img/login.svg')}}" class="image"></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 pt-4 pb-4">
                            <h3>Login</h3>
                            <form action="{{route('customer.login.store')}}" method="POST">
                                @csrf
                                <div class="row px-3">
                                    <label class="mb-0">
                                        <h6>Email Address</h6>
                                    </label>
                                    <input type="text"  class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter a valid email address">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="row px-3">
                                    <label class="mb-0">
                                        <h6>Password</h6>
                                    </label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter password" required>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="row px-3 mb-4">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="remember">
                                        <label for="remember" class="custom-control-label text-sm">Remember me</label>
                                    </div>
                                    <a href="#" class="forget ml-auto mb-0 text-sm">Forgot Password?</a>
                                </div>
                                <div class="row mb-3 px-3"><button type="submit" class="btn-cart">Login</button></div>
                                <div class="row mb-4 px-3">
                                    <small class="account-part">Don't have an account? <a class="text-danger" href="{{route('customer.register')}}">Register</a></small>
                                </div>
                            </form>
                            <div class="row px-3 mb-4">
                                <div class="line"></div>
                                <small class="or text-center">Or</small>
                                <div class="line"></div>
                            </div>
                            <div class="row mb-4 px-3">
                                <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                                <div class="icon-part facebook text-center mr-3">
                                    <a href="{{ route('login.via.driver','facebook') }}" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                                <div class="icon-part twitter text-center mr-3">
                                    <a href="{{ route('login.via.driver','google') }}" target="_blank">
                                        <i class="fab fa-google"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
