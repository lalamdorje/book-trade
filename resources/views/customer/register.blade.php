@extends('frontend.layouts.app')
@section('content')
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.php"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Login Page</li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="login-cart-section">
        <div class="container px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
            <div class="card card0 border-0">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="card1 pb-5">
                            <div class="row px-3 justify-content-center mt-5 pt-5 mb-5 border-line"><img src="{{asset('frontend/img/register.svg')}}" class="image"></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card2 card border-0 pt-4 pb-4">
                            <h3>Register Now</h3>
                            <form action="{{route('customer.register.store')}}" method="post">
                                @csrf
                                <div class="row ">
                                    <div class="col-md-6">
                                        <label class="mb-0">
                                            <h6>Full Name</h6>
                                        </label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Enter your full name">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label class="mb-0">
                                            <h6>Phone</h6>
                                        </label>
                                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Enter your phone">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row px-3">
                                    <label class="mb-0">
                                        <h6>Email</h6>
                                    </label>
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Enter your email">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <label class="mb-0">
                                            <h6>Password</h6>
                                        </label>
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" required placeholder="Enter password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label class="mb-0">
                                            <h6>Confirm Password</h6>
                                        </label>
                                        <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                                    </div>
                                </div>

                                <div class="row mb-3 px-3"><button type="submit" class="btn-cart mt-3">Register</button></div>
                                <div class="row mb-4 px-3">
                                    <small class="account-part">Already have an account? <a class="text-danger" href="{{route('customer.login')}}">Log In</a></small>
                                </div>
                            </form>
                            <div class="row px-3 mb-4">
                                <div class="line"></div>
                                <small class="or text-center">Or</small>
                                <div class="line"></div>
                            </div>
                            <div class="row mb-4 px-3">
                                <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                                <div class="icon-part facebook text-center mr-3">
                                    <a href="#" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                                <div class="icon-part twitter text-center mr-3">
                                    <a href="#" target="_blank">
                                        <i class="fab fa-google"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
