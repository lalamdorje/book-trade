@extends('frontend.layouts.app')
@section('styles')
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" referrerpolicy="no-referrer" />--}}
    <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>

    <style>
        .dropzone .dz-preview .dz-image img {
            display: block;
            height: 100%;
            width: 100%;
            object-fit: contain;
        }
    </style>
@endsection
@section('content')
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.php"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Post a Ad</li>
                </ol>
            </nav>
        </div>

        @if(Session::has('msg') || Session::has('error'))
            <div class="col-md-4 col-sm-5 float-right alert @if(Session::has('msg')) alert-success @else alert-danger @endif alert-dismissible m-2 mt-4 fade show" role="alert">
                {{Session::get('msg') ?? Session::get('error')}}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </section>
    <section class="ad section-padding profile">
        <div class="container">
            <div class="ad-form">
                <h4>Post Your Book</h4>
                <form action="{{isset($book)?route('frontend.post_ad.update', $book):route('frontend.post_ad.store')}}" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="type" value="frontend">
                    @csrf
                    @if(isset($book))
                        @method('PATCH')
                    @endif
                    <div class="form-group">
                        <label>Advertisement Type</label>
                        <select name="ad_type" id="ad_type" class="custom-select form-control-lg mb-3">
                            <option value="">Select Advertisement Type</option>
                            <option value="Single"
                                    @isset($book)@if($book->ad_type == "Single") selected @endif @endisset>Single
                            </option>
                            <option value="Bulk"
                                    @isset($book)@if($book->ad_type == "Bulk") selected @endif @endisset>Bulk
                            </option>
                        </select>
                        @if($errors->first('ad_type'))
                            <div class="text text-danger">
                                {{$errors->first('ad_type')}}
                            </div>
                        @endif
                    </div>
                    <div id="document-dropzone" class="needsclick dropzone"></div>
                    <div class="form-group">
                        <label>Title <span class="required">*</span> </label>
                        <input class="form-control form-control-lg mb-3" id="title" type="text" name="title"
                               value="{{old('title')?old('title'):(isset($book)?$book->title:'')}}"
                               placeholder="Your Book Title">
                        @if($errors->first('title'))
                            <div class="text text-danger">
                                {{$errors->first('title')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Category<span class="required">*</span> </label>
                        <select name="category_id" id="category_id" class="custom-select form-control-lg mb-3">
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                        @isset($book)@if($category->id == $book->category_id) selected @endif @endisset>{{$category->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('category_id'))
                            <div class="text text-danger">
                                {{$errors->first('category_id')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Sub-Category<span class="required">*</span> </label>
                        <select name="sub_category_id" id="sub_category_id"
                                class="custom-select form-control-lg mb-3">
                            <option value="">Select Sub-Category</option>
                            @foreach($subCategories as $subCategory)
                                <option value="{{$subCategory->id}}"
                                        @isset($book)@if($subCategory->id == $book->sub_category_id) selected @endif @endisset>{{$subCategory->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('sub_category_id'))
                            <div class="text text-danger">
                                {{$errors->first('sub_category_id')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group" id="ternaryCategory">
                        <label>Ternary Category</label>
                        <select name="ternary_category_id" id="ternary_category_id"
                                class="custom-select form-control-lg mb-3">
                            <option value="">Select Ternary-Category</option>
                            @foreach($ternaryCategories as $ternaryCategory)
                                <option value="{{$ternaryCategory->id}}"
                                        @isset($book)@if($ternaryCategory->id == $book->ternary_category_id) selected @endif @endisset>{{$ternaryCategory->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('ternary_category_id'))
                            <div class="text text-danger">
                                {{$errors->first('ternary_category_id')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Price Type</label>
                        <select name="price_type" id="price_type" class="custom-select form-control-lg mb-3">
                            <option value="">Select Price Type</option>
                            <option value="Fixed"
                                    @isset($book)@if($book->price_type == "Fixed") selected @endif @endisset>Fixed
                            </option>
                            <option value="Negotiable"
                                    @isset($book)@if($book->price_type == "Negotiable") selected @endif @endisset>
                                Negotiable
                            </option>
                            <option value="Price on call"
                                    @isset($book)@if($book->price_type == "Price on call") selected @endif @endisset>
                                Price on call
                            </option>
                            {{--                                <option value="Free" @isset($book)@if($book->price_type == "Free") selected @endif @endisset>Free</option>--}}
                        </select>
                        @if($errors->first('price_type'))
                            <div class="text text-danger">
                                {{$errors->first('price_type')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Price <span class="required">*</span> </label>
                        <input class="form-control form-control-lg mb-3" id="price" type="number" name="price"
                               value="{{old('price')?old('price'):(isset($book)?$book->price:'')}}"
                               placeholder="Your Book Price">
                        @if($errors->first('price'))
                            <div class="text text-danger">
                                {{$errors->first('price')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Item Condition</label>
                        <select name="item_condition" id="item_condition" class="custom-select form-control-lg mb-3">
                            <option value="">Select Item Condition</option>
                            <option value="New"
                                    @isset($book)@if($book->item_condition == "New") selected @endif @endisset>New
                            </option>
                            <option value="Used"
                                    @isset($book)@if($book->item_condition == "Used") selected @endif @endisset>Used
                            </option>
                            <option value="Like New"
                                    @isset($book)@if($book->item_condition == "Like New") selected @endif @endisset>
                                Like New
                            </option>
                        </select>
                        @if($errors->first('item_condition'))
                            <div class="text text-danger">
                                {{$errors->first('item_condition')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Book Edition <span class="required">*</span> </label>
                        <input class="form-control form-control-lg mb-3" id="book_edition" type="text"
                               name="book_edition"
                               value="{{old('book_edition')?old('book_edition'):(isset($book)?$book->book_edition:'')}}"
                               placeholder="Your Book Edition">
                        @if($errors->first('book_edition'))
                            <div class="text text-danger">
                                {{$errors->first('book_edition')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea id="mytextarea" class="form-control" name="description">{!! isset($book)?$book->description:(old('description') ?? '') !!}</textarea>
                        @if($errors->first('description'))
                            <div class="text text-danger">
                                *{{$errors->first('description')}}
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary submit-part">{{isset($book) ? 'Update' : 'Upload'}}</button>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js' type='text/javascript'></script>
    <script>
        $(document).ready(function () {
            $('#category_id').on('change', function () {
                let id = $(this).val();
                let category = $("#category_id option:selected").text();
                console.log(category)
                if (category == "Fiction" || category == "Non-Fiction") {
                    $('#ternaryCategory').removeClass('d-block').addClass('d-none');
                } else {
                    $('#ternaryCategory').removeClass('d-none').addClass('d-block');
                }
                $('#sub_category_id').empty();
                $('#sub_category_id').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                    type: 'GET',
                    url: '/getSubCategories/' + id,
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#sub_category_id').empty();
                        $('#sub_category_id').append(`<option value="" disabled selected>Select Sub-Category</option>`);
                        response.forEach(element => {
                            $('#sub_category_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                        });
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#sub_category_id').on('change', function () {
                let id = $(this).val();
                $('#ternary_category_id').empty();
                $('#ternary_category_id').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                    type: 'GET',
                    url: '/getTernaryCategories/' + id,
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log(response);
                        $('#ternary_category_id').empty();
                        $('#ternary_category_id').append(`<option value="" disabled selected>Select Ternary-Category</option>`);
                        response.forEach(element => {
                            $('#ternary_category_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                        });
                    }
                });
            });
        });
    </script>
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('books.storeMedia') }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                console.log(file);
                $('form').append('<input type="hidden" name="images[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('form').find('input[name="images[]"][value="' + name + '"]').remove()
            },
            init: function () {
                @if(isset($book) && $book->images)
                var files =
                    {!! json_encode($book->images) !!}
                    for(var i in files)
                {
                    var file = files[i]
                    console.log(file);
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="images[]" value="' + file.file_name + '">')
                    $('.dz-image img').attr('src', file.original_url)
                }
                @endif
            }
        }
    </script>
@endsection
