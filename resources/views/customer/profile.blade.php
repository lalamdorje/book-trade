@extends('frontend.layouts.app')
@section('content')
    @if(Session::has('msg') || Session::has('error'))
        <div
            class="col-md-4 col-sm-5 float-right alert @if(Session::has('msg')) alert-success @else alert-danger @endif alert-dismissible m-2 mt-4 fade show"
            role="alert">
            {{Session::get('msg') ?? Session::get('error')}}.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.php"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="profile py-2">
        <div class="container">
            <div class="row no-gutters h-100">
                <div class="col-md-4 left-profile">
                    <div class="profile-wrapper">
                        <div class="user-image">
                            <img src="https://avatars.dicebear.com/api/bottts/your-custom-seed.svg?colors[]=lightBlue"
                                 alt=""/>
                        </div>
                        <div class="user-detail text-center mt-3">
                            <h5>{{$customer->name}}</h5>
                            <p class="text-secondary">{{$customer->email}}</p>
                            <p class="text-secondary">{{$customer->phone}}</p>
                            {{--
                            <p class="text-secondary">{{$customer->address}}</p>
                            --}}
                        </div>
                        <button type="button" class="btn  btn btn-outline-success mt-2" id="formButton">Edit Profile
                        </button>
                    </div>
                    <div class="edit-profile" id="edit-profile">
                        <h5 class="mb-3">Update Profile</h5>
                        <form action="{{route('customer.profile.update')}}" method="POST">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="fname">Full Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$customer->name}}"
                                           id="fname" placeholder="Full Name"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Phone Number</label>
                                    <input type="text" class="form-control" name="phone" value="{{$customer->phone}}"
                                           id="inputPassword4" placeholder="Phone Number"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email Address</label>
                                    <input type="email" class="form-control" name="email" value="{{$customer->email}}"
                                           id="email" placeholder="Email Address"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Address</label>
                                    <input type="text" class="form-control" name="address"
                                           value="{{$customer->address}}" id="address" placeholder="Address"/>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary submit-part">Update Profile</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="edit-profile">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab"
                                   aria-controls="pills-all" aria-selected="false">All Ads</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                   role="tab" aria-controls="pills-home" aria-selected="true">Your Ad</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                   role="tab" aria-controls="pills-profile" aria-selected="false">Sold/Expired Ad</a>
                            </li>
                            <div class="ad-profile-btn ml-auto">
                                <a href="{{route('frontend.post_ad')}}"> <i class="fas fa-plus mr-1"></i> Post Ad</a>
                            </div>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                 aria-labelledby="pills-home-tab">
                                <div class="row">
                                    @foreach($allBooks as $book)
                                        <div class="col-md-4">
                                            <div class="product-wrapper">
                                                <div class="product-img">
                                                    <a href="{{route('frontend.book.detail', $book->id)}}">
                                                        <img src="{{$book->images[0]->getFullUrl()}}" alt="book"
                                                             class="primary"/>
                                                    </a>
                                                    @if($book->is_featured == 1)
                                                        <div class="product-flag">
                                                            <ul>
                                                                <li><span class="sale">Featured</span></li>
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="product-details text-center">
                                                    <div
                                                        class="badge badge-info px-2 py-2 mt-2">{{$book->category->name}}</div>
                                                    <h4>
                                                        <a href="{{route('frontend.book.detail', $book->id)}}">{{$book->title}}</a>
                                                    </h4>
                                                    <div class="product-prices mb-1">
                                                        <ul class="d-flex justify-content-center">
                                                            <li>{{show_price_format($book->price)}}</li>
                                                            <li class="old-price">({{$book->price_type}})</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-footer">
                                                    <div class="footer-btn d-flex pt-1">
                                                        <div class="w-50 mr-2">
                                                            <button type="button" class="btn btn-danger w-100"
                                                                    data-toggle="modal" data-target="#bookSoldModal"
                                                                    data-id="{{$book->id}}">Sold
                                                            </button>
                                                        </div>
                                                        <div class="w-50">
                                                            <a href="{{route('frontend.post_ad.edit', $book)}}">
                                                                <button type="button" class="btn btn-warning w-100">
                                                                    Edit
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade" id="bookSoldModal" tabindex="-1"
                                                         role="dialog" aria-labelledby="bookSoldModalTitle"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                                                        Are you sure you have sold the book
                                                                        "{{$book->title}}"?</h5>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary"
                                                                            id="submitSoldBook">Yes
                                                                    </button>
                                                                    <button type="button" class="btn btn-danger"
                                                                            data-dismiss="modal" aria-label="Close">No
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                 aria-labelledby="pills-profile-tab">
                                <div class="row">
                                    @forelse($soldBooks as $soldBook)
                                        <div class="col-md-4">
                                            <div class="product-wrapper">
                                                <div class="product-img">
                                                    <a href="{{route('frontend.book.detail', $soldBook->id)}}">
                                                        <img src="{{$soldBook->images[0]->getFullUrl()}}" alt="book"
                                                             class="primary"/>
                                                    </a>
                                                    <div class="product-flag">
                                                        <ul>
                                                            <li><span class="sale">Sold</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-details text-center">
                                                    <div
                                                        class="badge badge-info px-2 py-2 mt-2">{{$soldBook->category->name}}</div>
                                                    <h4>
                                                        <a href="{{route('frontend.book.detail', $soldBook->id)}}">{{$soldBook->title}}
                                                            k</a></h4>
                                                    <div class="product-prices mb-1">
                                                        <ul class="d-flex justify-content-center">
                                                            <li>{{show_price_format($soldBook->price)}}0</li>
                                                            <li class="old-price">({{$soldBook->price_type}})</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-footer">
                                                    <form action="{{route('frontend.book.delete', $soldBook)}}"
                                                          method="POST" onsubmit="return confirm('Are you sure?')">
                                                        @csrf @method('DELETE')
                                                        <button type="submit" class="btn btn-danger w-100">Delete
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="no-image">
                                            <img class="no-img" src="{{asset('frontend/img/noimage.jpg')}}" alt=""/>
                                            <h4><strong>No Books Available!!</strong></h4>
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                                <div class="row">
                                    @foreach($books as $b)
                                        <div class="col-md-4">
                                            <div class="product-wrapper">
                                                <div class="product-img">
                                                    <a href="{{route('frontend.book.detail', $b->id)}}">
                                                        <img src="{{$b->images[0]->getFullUrl()}}" alt="book"
                                                             class="primary"/>
                                                    </a>
                                                    @if($b->is_featured == 1)
                                                        <div class="product-flag">
                                                            <ul>
                                                                <li><span class="sale">Featured</span></li>
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="product-details text-center">
                                                    <div
                                                        class="badge badge-info px-2 py-2 mt-2">{{$b->category->name}}</div>
                                                    <h4>
                                                        <a href="{{route('frontend.book.detail', $b->id)}}">{{$b->title}}</a>
                                                    </h4>
                                                    <div class="product-prices mb-1">
                                                        <ul class="d-flex justify-content-center">
                                                            <li>{{show_price_format($b->price)}}</li>
                                                            <li class="old-price">({{$b->price_type}})</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection @section('js')
    <script>
        $(document).ready(function () {
            $("#bookSoldModal").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data("id");
                $("#submitSoldBook").data("id", id);
                console.log(id);
            });
            $("#submitSoldBook").click(function () {
                const id = $(this).data("id");
                console.log(id);
                $.ajax({
                    type: "GET",
                    url: "/book/sold/" + id,
                    success: function (response) {
                        $("#bookSoldModal").modal("hide");
                        window.location.reload();
                    },
                });
            });
        });
        $(document).ready(function () {
            $("#formButton").click(function () {
                $("#edit-profile").toggle();
            });
        });
    </script>
@endsection
