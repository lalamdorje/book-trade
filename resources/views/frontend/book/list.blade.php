@extends('frontend.layouts.app')
<?php
$booksCount = \App\Models\Book::isNotSold()->count();
?>
@section('content')
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('frontend.home')}}"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">List Page</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="book-list-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="accordion" id="accordion" role="tablist">
                        <div class="card">
                            <div class="card-header" role="tab" id="heading-1">
                                <h6 class="mb-0"> <a data-toggle="collapse" href="#collapse-1" aria-expanded="true" aria-controls="collapse-1" data-abc="true" class="collapsed"> Categories </a> </h6>
                            </div>
                            <div id="collapse-1" class="collapse show" role="tabpanel" aria-labelledby="heading-1" data-parent="#accordion" style="">
                                <div class="card-body">
                                    <div class="category-list">
                                        <ul>
                                            @foreach($categories as $cat)
                                            <li>
                                                <a href="{{route('frontend.book.category.list', $cat)}}">
                                                    <i class="fa fa-hand-o-right" aria-hidden="true"></i> {{$cat->name}}
                                                </a>
                                            </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
{{--                            <div class="card-header" role="tab" id="heading-5">--}}
{{--                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapse-5" aria-expanded="false" aria-controls="collapse-5" data-abc="true"> Filter By Price </a> </h6>--}}
{{--                            </div>--}}
{{--                            <div id="collapse-5" class="collapse" role="tabpanel" aria-labelledby="heading-5" data-parent="#accordion">--}}
{{--                                <div class="card-body">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <div class="range-slider">--}}
{{--                                            <input value="1000" min="1000" max="50000" step="500" type="range">--}}
{{--                                            <input value="50000" min="1000" max="50000" step="500" type="range">--}}
{{--                                            <span class="rangeValues"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="section-title mt-4 mb-3">
                        <h3>Featured Books</h3>
                    </div>
                    <div class="book-list">
                        <ul>
                            @foreach($featuredBooks as $featuredBook)
                            <li>
                                <div class="single-most-product bd mb-18">
                                    <div class="most-product-img">
                                        <a href="{{route('frontend.book.detail', $featuredBook->id)}}"><img src="{{$featuredBook->images[0]->getFullUrl()}}" alt="book" /></a>
                                    </div>
                                    <div class="most-product-content">

                                        <h4><a href="{{route('frontend.book.detail', $featuredBook->id)}}">{{$featuredBook->title}}</a></h4>
                                        <div class="product-price">
                                            <ul>
                                                <li>{{ show_price_format($featuredBook->price)}}</li>
                                                <li class="old-price">({{$featuredBook->price_type}})</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row align-items-center">
                        <div class="col-md-7">
                            @if(isset($category))
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$category->name}}</div>
                                </div>
                            @elseif(isset($subCategory))
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$subCategory->category->name}}</div>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$subCategory->name}}</div>
                                </div>
                            @elseif(isset($ternaryCategory))
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$ternaryCategory->category->name}}</div>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$ternaryCategory->sub_category->name}}</div>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$ternaryCategory->name}}</div>
                                </div>
                            @elseif(isset($searchedCategory))
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{$searchedCategory->name}}</div>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{request()->title}}</div>
                                    @if(request()->address != '')
                                        <div class="badge badge-info px-2 py-2 ml-3"> {{request()->address}}</div>
                                    @endif
                                </div>
                            @elseif(isset(request()->search))
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> {{request()->search}}</div>
                                </div>
                            @else
                                <div class="show d-flex align-items-center">
                                    <h4>Showing 1–20 of {{$booksCount}} results</h4>
                                    <div class="badge badge-info px-2 py-2 ml-3"> All Books</div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <div class="sort">
                                <div class="form-group">
                                    <select class="custom-select" id="exampleFormControlSelect1">
                                        <option>Default sorting</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by newness</option>
                                        <option>Sort by price : low to high</option>
                                        <option>Sort by price : high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="sort">
                                <div class="form-group">
                                    <select class="custom-select" id="exampleFormControlSelect1">
                                        <option>Show 10</option>
                                        <option>Show 20</option>
                                        <option>Show 30</option>
                                        <option>Show 40</option>
                                        <option>Show 50</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        @forelse($books as $book)
                            <div class="col-md-3">
                                <div class="product-wrapper">
                                    <div class="product-img">
                                        <a href="{{route('frontend.book.detail', $book->id)}}">
                                            <img src="{{$book->images[0]->getFullUrl()}}" alt="book" class="primary" />
                                        </a>
                                        @if($book->is_featured == 1)
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="sale">Featured</span></li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="product-details text-center mt-1">
                                        <div class="badge badge-info px-2 py-2">{{$book->category->name}}</div>
                                        <h4><a href="{{route('frontend.book.detail', $book->id)}}">{{$book->title}}</a></h4>
                                        <div class="product-price mb-1">
                                            <ul>
                                                <li>{{show_price_format($book->price)}}</li>
                                                <li class="old-price">({{$book->price_type}})</li>
                                            </ul>
                                        </div>
                                    </div>
{{--                                    <div class="product-link text-center">--}}
{{--                                        <div class="product-button">--}}
{{--                                            <a href="detail.php" title="Add to cart">View Detail</a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        @empty
                            <div class="no-image">
                                <img class="no-img" src="{{asset('frontend/img/noimage.jpg')}}" alt="">
                                <h4> <strong>No Books Available!!</strong></h4>
                            </div>
                        @endforelse
                    </div>
                    <div class="row justify-content-center">
                        <nav>
                           {!! $books->links() !!}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
