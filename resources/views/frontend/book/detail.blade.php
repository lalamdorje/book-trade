@extends('frontend.layouts.app')
@section('content')
    <section class="book-breadcumb-section">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('frontend.home')}}"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail Page</li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="book-detail-page section-padding pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                        <div class="carousel-inner">
                            @foreach($book->images as $key => $image)
                                <div class="carousel-item @if($key == 0) active @endif"><img class="detail-img" src="{{$image->getFullUrl()}}" alt="book" />
                                @if($book->is_featured == 1)
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="sale">Featured</span></li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
                        <a class="carousel-control-next" href="#custCarousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
                        <ol class="carousel-indicators list-inline">
                            @foreach($book->images as $key => $image)
                                <li class="list-inline-item @if($key == 0) active @endif">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarousel"> <img src="{{$image->getFullUrl()}}" class="img-fluid" /> </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="detail-desc">
                        <div class="head-rate-desc">
                            <h4>{{$book->title}}</h4>
                            <div class="rate-price">
                                <div class="price-part">
                                    <p>{{show_price_format($book->price)}} ({{$book->price_type}}) </p>
                                </div>
                            </div>
                            <div class="detail-table">
                            <table class="table table-hover mt-3">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Title:</th>
                                        <td>{{$book->title}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posted By:</th>
                                        <td>{{$book->customer->name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Book Edition:</th>
                                        <td>{{$book->book_edition}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Posted date:</th>
                                        <td>{{$book->created_at->format('d F, Y')}}</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Book Condition:</th>
                                        <td>
                                            @if($book->item_condition == "New")
                                                <span class="shadow-none badge badge-success" style="color: white">{{$book->item_condition}}</span></h6>
                                            @elseif($book->item_condition == 'Like New')
                                                <span class="shadow-none badge badge-warning" style="color: white">{{$book->item_condition}}</span></h6>
                                            @elseif($book->item_condition == 'Used')
                                                <span class="shadow-none badge badge-danger" style="color: white">{{$book->item_condition}}</span></h6>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Price Type:</th>
                                        <td>{{$book->price_type}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Price:</th>
                                        <td>{{show_price_format($book->price)}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="book-desc">
                                <p>
                                    {!! $book->description !!}
                                </p>
                            </div>
                        </div>
                        <div class="detail-btn mt-3">
                            <a href="mailto:{{$book->customer->email}}"><button class="btn main-btn"><i class="fas fa-comments mr-2"></i>Send Email</button></a>
                            <button class="btn main-btn ph-btn ml-3"><i class="fas fa-mobile-alt mr-2"></i>{{$book->customer->phone}}</button>
                        </div>
{{--                        <div class="share mt-4">--}}
{{--                            <span>Share This:</span>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="#" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#" class="instagram" target="_blank"><i class="fab fa-instagram"></i></a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="book-description-tab">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Book Details</a>
                            </li>
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">Reviews (0)</a>--}}
{{--                            </li>--}}
                        </ul>
                        <div class="tab-content book-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                <p>
                                    {!! $book->description !!}
                                </p>
                            </div>
                            <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                                <table class="table table-hover table-borderless text-center mt-3">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Title:</th>
                                        <td>{{$book->title}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Book Edition:</th>
                                        <td>{{$book->book_edition}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Published date:</th>
                                        <td>{{$book->created_at->format('d F, Y')}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Published By:</th>
                                        <td>{{$book->customer->name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Book Condition:</th>
                                        <td>
                                            @if($book->item_condition == "New")
                                                <span class="shadow-none badge badge-success" style="color: white">{{$book->item_condition}}</span></h6>
                                            @elseif($book->item_condition == 'Like New')
                                                <span class="shadow-none badge badge-warning" style="color: white">{{$book->item_condition}}</span></h6>
                                            @elseif($book->item_condition == 'Used')
                                                <span class="shadow-none badge badge-danger" style="color: white">{{$book->item_condition}}</span></h6>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Price Type:</th>
                                        <td>{{$book->price_type}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Price:</th>
                                        <td>{{show_price_format($book->price)}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
{{--                            <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">--}}
{{--                                <div class="row mt-3">--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <div class="cust-rev">--}}
{{--                                            <h5>Customer Review</h5>--}}
{{--                                        </div>--}}
{{--                                        <div class="customer-review d-flex align-items-center">--}}
{{--                                            <span class="rate">4.6</span>--}}
{{--                                            <div class="ml-3 h6 mb-0">--}}
{{--                                                <span class="rev">3,714 reviews</span>--}}
{{--                                                <div class="text-yellow-darker">--}}
{{--                                                    <small class="fa fa-star"></small>--}}
{{--                                                    <small class="fa fa-star"></small>--}}
{{--                                                    <small class="fa fa-star"></small>--}}
{{--                                                    <small class="fa fa-star"></small>--}}
{{--                                                    <small class="fa fa-star"></small>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="d-flex btn-review">--}}
{{--                                            <button>See All Reviews</button>--}}
{{--                                            <button>Write A Review</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <div class="star-rating-list">--}}
{{--                                            <ul>--}}
{{--                                                <li>--}}
{{--                                                    <div class="row align-items-center">--}}
{{--                                                        <div class="col-md-2"><span class="star">5 Stars</span></div>--}}
{{--                                                        <div class="col-md-8">--}}
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar" style="width: 70%;"></div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-md-2">--}}
{{--                                                            <span class="num">50</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="row align-items-center">--}}
{{--                                                        <div class="col-md-2"><span class="star">4 Stars</span></div>--}}
{{--                                                        <div class="col-md-8">--}}
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar" style="width: 40%;"></div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-md-2">--}}
{{--                                                            <span class="num">50</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="row align-items-center">--}}
{{--                                                        <div class="col-md-2"><span class="star">3 Stars</span></div>--}}
{{--                                                        <div class="col-md-8">--}}
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar" style="width: 30%;"></div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-md-2">--}}
{{--                                                            <span class="num">50</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="row align-items-center">--}}
{{--                                                        <div class="col-md-2"><span class="star">2 Stars</span></div>--}}
{{--                                                        <div class="col-md-8">--}}
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar" style="width: 50%;"></div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-md-2">--}}
{{--                                                            <span class="num">50</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="row align-items-center">--}}
{{--                                                        <div class="col-md-2"><span class="star">1 Stars</span></div>--}}
{{--                                                        <div class="col-md-8">--}}
{{--                                                            <div class="progress">--}}
{{--                                                                <div class="progress-bar" style="width: 10%;"></div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-md-2">--}}
{{--                                                            <span class="num">50</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="review-list">--}}
{{--                                            <span>1-5 of 44 reviews</span>--}}
{{--                                            <ul>--}}
{{--                                                <li>--}}
{{--                                                    <div class="d-flex justify-content-between mt-4">--}}
{{--                                                        <h3>Get the best seller at a great price.</h3>--}}
{{--                                                        <div class="text-yellow-darker">--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <h6>February 22, 2020</h6>--}}
{{--                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
{{--                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
{{--                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
{{--                                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
{{--                                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non--}}
{{--                                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>--}}

{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="d-flex justify-content-between mt-4">--}}
{{--                                                        <h3>Get the best seller at a great price.</h3>--}}
{{--                                                        <div class="text-yellow-darker">--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                            <small class="fa fa-star"></small>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <h6>February 22, 2020</h6>--}}
{{--                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
{{--                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
{{--                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
{{--                                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
{{--                                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non--}}
{{--                                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>--}}

{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="related-books ">
        <div class="container">
            <h3 class="text-center mb-4">Related Books</h3>
            <div class="col-md-12">
                <div class="related-slider owl-carousel owl-theme ">
                    @foreach($relatedBooks as $relatedBook)
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="{{route('frontend.book.detail', $relatedBook->id)}}">
                                    <img src="{{$relatedBook->images[0]->getFullUrl()}}" alt="book" class="primary" />
                                </a>
                                @if($relatedBook->is_featured == 1)
                                    <div class="product-flag">
                                        <ul>
                                            <li><span class="sale">featured</span></li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <div class="product-details text-center">
{{--                                <div class="badge badge-info px-2 py-2">Engineering</div>--}}
                                <h4><a href="{{route('frontend.book.detail', $relatedBook->id)}}">{{$relatedBook->title}}</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>{{show_price_format($relatedBook->price)}}</li>
                                        <li class="old-price">({{$relatedBook->price_type}})</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
