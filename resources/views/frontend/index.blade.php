@extends('frontend.layouts.app')
@section('content')
    <section class="slider-area" style="background-image: url({{asset('frontend/img/slider/1.jpg')}});">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="slider-active">
                        <div class="single-slider bg-img">
                            <div class="slider-content">
                                <h2 class="text-white">{{$siteSetting->name}}</h2>
                                <p class="text-white">{!! $siteSetting->description !!}</p>
                                <a href="{{route('frontend.post_ad')}}">Post Ad</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="banner-search-form">
                        <h3>Search books You Looking For</h3>
                        <form class="form-join" action="{{route('frontend.book.search')}}" method="GET">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Category</label>
                                <select class="custom-select" name="category_id" required>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input class="form-control" autocomplete="off" name="title"
                                       placeholder="What Are You Looking For..." type="text" required/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Location</label>
                                <input class="form-control pac-target-input" name="address" id="sb_user_address"
                                       placeholder="Location..." type="text" autocomplete="off"/>
                            </div>
                            <div class="search-form-btn mt-4">
                                <button class="main-btn w-100 border-0" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
