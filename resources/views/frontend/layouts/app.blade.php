<!doctype html>
<html lang="en">

@include('frontend.layouts.header')

<body>
<div>
    @include('frontend.layouts.navbar')
</div>
<main id="content" class="site-main">
    @yield('content')
</main>
@include('frontend.layouts.footer')

</body>

</html>




