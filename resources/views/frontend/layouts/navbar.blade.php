@php
$categories = \App\Models\Category::all();
$siteSetting = \App\Models\SiteSetting::first();
@endphp

<header class="site-header header-primary">
    <div class="top-header">
        <div class="container">
            <div class="row align-items-center  py-2">
                <div class="col-lg-3 col-md-3 col-12 d-none d-lg-block">
                    <div class="top-contact">
                        <a href="tel:{{$siteSetting->phone}}"> <i class="fa fa-phone"></i><span>{{$siteSetting->phone}}</span> </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="header-search">
                        <form action="{{route('frontend.book.search')}}" method="get" id="searchBookForm">
                            @csrf
                            <input type="text" name="search" placeholder="Search book here..." value="{{old('search')?old('search'): request()->search}}"/>
                            <a href="javascript:{}" onclick="document.getElementById('searchBookForm').submit();"><i class="fa fa-search"></i></a>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-12 d-flex align-items-center justify-content-center">
                    @if(!auth()->guard('customer')->check())
                        <div class="account-area text-right">
                            <ul>
                                <li><a href="{{route('customer.login')}}">Sign in</a></li>
                                <li><a href="{{route('customer.register')}}">Register Now</a></li>
                            </ul>
                        </div>
                    @endif
                    @auth('customer')
                        <div class="my-cart d-flex ml-3">
                            <div class="header-user-icon">
                                <i class="far fa-user mr-1" style="cursor: pointer"></i>
                                {{ auth()->guard('customer')->user()->name }}
                                <ul class="dropdown">
                                    <li><a href="{{route('customer.home')}}">Profile</a></li>
                                    <li><a href="{{route('frontend.post_ad')}}">Post Ad</a></li>
                                    <li>
                                        <a href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item ai-icon">
                                            <span class="ml-2">Logout </span>
                                        </a>
                                        <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endauth
                </div>
            </div>
        </div>

        <div class="bottom-header header-top">
            <div class="container d-flex  align-items-center position-relative">
                <div class="logo-area">
                    <a href="{{ route('frontend.home') }}"><img src="{{ asset('frontend/img/logo/logo1.png ')}}" alt="logo" /></a>
                </div>
                <div class="main-navigation d-none d-lg-block mx-auto">
                    <nav id="navigation" class="navigation">
                        <ul>
                            <li>
                                <a href="{{ route('frontend.home') }}">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{route('frontend.book.list')}}">Book Categories</a>
{{--                                <i class="fa-solid fa-caret-right"></i>--}}
                                <ul>
                                    @foreach($categories as $category)
                                    <li class="menu-item-has-children sub-child">
                                        <a href="{{route('frontend.book.category.list', $category)}}">{{$category->name}} <i class="fas fa-caret-right float-right"></i></a>
                                        <ul>
                                            @foreach($category->sub_categories as $subCategory)
                                            <li class="menu-item-has-children sub-child">
                                                <a href="{{route('frontend.book.subcategory.list', $subCategory)}}">{{$subCategory->name}}
                                                    @if($subCategory->ternary_categories->isNotEmpty())
                                                        <i class="fas fa-caret-right float-right"></i>
                                                    @endif
                                                </a>
                                                @if($subCategory->ternary_categories->isNotEmpty())
                                                    <ul>
                                                        @foreach($subCategory->ternary_categories as $ternaryCategory)
                                                        <li>
                                                            <a href="{{route('frontend.book.ternary_category.list', $ternaryCategory)}}">{{$ternaryCategory->name}}</a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a href="{{ route('frontend.contact') }}">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="mobile-menu-container"></div>
    </div>
</header>


