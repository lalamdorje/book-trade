@php    
$siteSetting = \App\Models\SiteSetting::first();
@endphp 
 <!-- ##### Footer Area Start ##### -->
 <footer class="footer-area">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100 d-flex flex-wrap align-items-center justify-content-between">
                    <!-- Footer Social Info -->
                    <div class="footer-social-info text-right">
                        <a href="{{$siteSetting->facebook_link}}"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                        <a href="{{$siteSetting->twitter_link}}"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                        <a href="{{$siteSetting->linkedin_link}}"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
                        <a href="{{$siteSetting->instagram_link}}"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                    </div>
                    <!-- Footer Logo -->
                    <a href="{{route('frontend.home')}}">
                        <img src="{{asset('frontend/img/logo/logo1.png')}}" alt="">
                    </a>
                    <!-- Copywrite -->
                        Copyright  All rights reserved | Made with <i class="fas fa-heart" aria-hidden="true"></i> by <a href="https://www.softmahal.com" target="_blank"><p>Softmahal Technologies</p></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->
<!-- <div id="back-top">
   <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div> -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{asset('frontend/assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendors/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendors/masonry/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendors/slick/slick.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('frontend/assets/vendors/owlcarousel/js/owl.carousel.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="{{asset('frontend/assets/js/custom.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/custom.js')}}"></script>
@yield('js')
