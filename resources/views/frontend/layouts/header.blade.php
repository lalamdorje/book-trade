<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="{{asset('frontend/assets/images/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/vendors/bootstrap/css/bootstrap.min.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/vendors/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/vendors/jquery-ui/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/vendors/owlcarousel/css/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/vendors/owlcarousel/css/owl.theme.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/vendors/owlcarousel/css/owl.theme.default.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/vendors/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/vendors/slick/slick-theme.css')}}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/style.css')}}">
    @yield('styles')
    <title>Book Trade</title>
</head>
