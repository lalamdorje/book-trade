@extends('frontend.layouts.app')
@section('content')
<section class="book-breadcumb-section">
    <div class="container text-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><i class="fa fa-home"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Contact Page</li>
            </ol>
        </nav>
    </div>
</section>
<section class="contact-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="form-contact">
                    <h3>Contact Us</h3>
                    <form action="{{route('frontend.contact.store')}}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fname">Full Name</label>
                                <input type="text" class="form-control" id="fname" name="name" placeholder="Full Name" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Phone Number</label>
                                <input type="text" class="form-control" id="inputPassword4" name="phone" placeholder="Phone Number" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" />
                        </div>
                        <div class="form-group">
                            <label for="email">Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary submit-part">Submit Now</button>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-add">
                    <h3>Get In Touch</h3>
                    <p>We are available to help and advice you with any difficulties.</p>
                    <ul>
                        <li>
                            <div class="media">
                              <i class="fa fa-map-marker"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Address</h5>
                                <p>{{ $siteSettings->address }}</p>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <i class="fa fa-phone"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Phone</h5>
                                <p>{{ $siteSettings->phone }}</p>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <i class="fa fa-envelope"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Email</h5>
                                <p>{{ $siteSettings->email }}</p>
                              </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div class="map">
                    <iframe
                    src={{$siteSettings->iframe}}
                    width="100%"
                    height="450"
                    style="border: 0;"
                    allowfullscreen=""
                    loading="lazy"
                    ></iframe>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
