<?php include 'partial/header.php'; ?>
<section class="book-breadcumb-section">
            <div class="container text-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">List Page</li>
                    </ol>
                </nav>
            </div>
        </section>

<section class="book-list-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="accordion" id="accordion" role="tablist">
                        <div class="card">
                            <div class="card-header" role="tab" id="heading-1">
                                <h6 class="mb-0"> <a data-toggle="collapse" href="#collapse-1" aria-expanded="true" aria-controls="collapse-1" data-abc="true" class="collapsed"> Categories </a> </h6>
                            </div>
                            <div id="collapse-1" class="collapse show" role="tabpanel" aria-labelledby="heading-1" data-parent="#accordion" style="">
                                <div class="card-body">
                                    <div class="category-list">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                   <i class="fa fa-hand-o-right" aria-hidden="true"></i> Bachelors
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                   <i class="fa fa-hand-o-right" aria-hidden="true"></i> Masters
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                   <i class="fa fa-hand-o-right" aria-hidden="true"></i> +2
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                   <i class="fa fa-hand-o-right" aria-hidden="true"></i> A-level
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                   <i class="fa fa-hand-o-right" aria-hidden="true"></i> School
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" role="tab" id="heading-5">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapse-5" aria-expanded="false" aria-controls="collapse-5" data-abc="true"> Filter By Price </a> </h6>
                            </div>
                            <div id="collapse-5" class="collapse" role="tabpanel" aria-labelledby="heading-5" data-parent="#accordion">
                                <div class="card-body">
                                   <div class="form-group">
                                      <div class="range-slider">
                                         <input value="1000" min="1000" max="50000" step="500" type="range">
                                         <input value="50000" min="1000" max="50000" step="500" type="range">
                                         <span class="rangeValues"></span>
                                      </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="section-title mt-4 mb-3">
                    <h3>Featured Books</h3>
                </div>
                <div class="book-list">
                    <ul>
                        <li>
                            <div class="single-most-product bd mb-18">
                                <div class="most-product-img">
                                    <a href="detail.php"><img src="img/product/10.jpg" alt="book" /></a>
                                </div>
                                <div class="most-product-content">

                                    <h4><a href="detail.php">Voyage Yoga Bag</a></h4>
                                    <div class="product-price">
                                        <ul>
                                            <li>$30.00</li>
                                            <li class="old-price">(fixed price)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="single-most-product bd mb-18">
                                <div class="most-product-img">
                                    <a href="detail.php"><img src="img/product/11.jpg" alt="book" /></a>
                                </div>
                                <div class="most-product-content">

                                    <h4><a href="detail.php">Voyage Yoga Bag</a></h4>
                                    <div class="product-price">
                                        <ul>
                                            <li>$30.00</li>
                                            <li class="old-price">(fixed price)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="single-most-product bd mb-18">
                                <div class="most-product-img">
                                    <a href="detail.php"><img src="img/product/13.jpg" alt="book" /></a>
                                </div>
                                <div class="most-product-content">

                                    <h4><a href="detail.php">Voyage Yoga Bag</a></h4>
                                    <div class="product-price">
                                        <ul>
                                            <li>$30.00</li>
                                            <li class="old-price">(fixed price)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="single-most-product bd mb-18">
                                <div class="most-product-img">
                                    <a href="detail.php"><img src="img/product/14.jpg" alt="book" /></a>
                                </div>
                                <div class="most-product-content">

                                    <h4><a href="detail.php">Voyage Yoga Bag</a></h4>
                                    <div class="product-price">
                                        <ul>
                                            <li>$30.00</li>
                                            <li class="old-price">(fixed price)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row align-items-center">
                    <div class="col-md-7">
                        <div class="show d-flex align-items-center">
                            <h4>Showing 1–20 of 100 results</h4>
                             <div class="badge badge-info px-2 py-2 ml-3"> Engineering</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sort">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                              <option>Default sorting</option>
                              <option>Sort by popularity</option>
                              <option>Sort by newness</option>
                              <option>Sort by price : low to high</option>
                              <option>Sort by price : high to low</option>
                            </select>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                        <div class="sort">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                              <option>Show 10</option>
                              <option>Show 20</option>
                              <option>Show 30</option>
                              <option>Show 40</option>
                              <option>Show 50</option>
                            </select>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="#">
                                    <img src="img/product/7.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                                <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/5.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/3.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/8.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/9.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/10.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/11.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/13.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/14.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="detail.php">
                                    <img src="img/product/14.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">Featured</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <nav>
                      <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="">Previous</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                      </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'partial/footer.php'; ?>
