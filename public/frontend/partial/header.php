<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendors/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="assets/vendors/owlcarousel/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="assets/vendors/owlcarousel/css/owl.theme.css" />
    <link rel="stylesheet" href="assets/vendors/owlcarousel/css/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/vendors/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/vendors/slick/slick-theme.css">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css' rel='stylesheet'>

    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Book Trade</title>
</head>

<body>
<div>
<header class="site-header header-primary">
    <div class="top-header">
        <div class="container">
            <div class="row align-items-center  py-2">
                <div class="col-lg-3 col-md-3 col-12">
                    <div class="top-contact">
                        <a href="tel:+977-98412345678"> <i class="fa fa-phone"></i><span>+977-98412345678</span> </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="header-search">
                        <form action="#">
                            <input type="text" placeholder="Search book here..." />
                            <a href="#"><i class="fa fa-search"></i></a>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-12 d-flex align-items-center justify-content-end">
                    <div class="account-area text-right">
                        <ul>
                            <li><a href="login.php">Sign in</a></li>
                            <li><a href="register.php">Register Now</a></li>
                        </ul>
                    </div>
                    <div class="my-cart d-flex ml-3">
                    <div class="post-ad">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="header-user-icon">
                        <a class="dropdown-toggle" href="#"><i class="far fa-user"></i></a>
                        <ul class="dropdown">
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="post_ad.php">Post Ad</a></li>
                            <li><a href="#">Log out</a></li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="bottom-header header-top">
            <div class="container d-flex  align-items-center position-relative">
                <div class="logo-area">
                    <a href="index.php"><img src="img/logo/logo1.png" alt="logo" /></a>
                </div>
                <div class="main-navigation d-none d-lg-block mx-auto">
                    <nav id="navigation" class="navigation">
                        <ul>
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="listing.php">Book Categories</a>
                                <ul>
                                    <li class="menu-item-has-children sub-child">
                                        <a href="listing.php">Bachelors</a>
                                        <ul>
                                            <li>
                                                <a href="listing.php.php">Engineering</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">Medical</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">IT</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">BA</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">BSC</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">BHM</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">BBA</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">BBS</a>
                                            </li>
                                            <li>
                                                <a href="listing.php">Agriculture & Forestry</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children sub-child">
                                        <a href="listing.php">Masters</a>
                                        <ul>
                                            <li><a href="listing.php">MBA</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children sub-child">
                                        <a href="listing.php">+2</a>
                                        <ul>
                                            <li><a href="listing.php">Management</a></li>
                                            <li><a href="listing.php">Science</a></li>
                                            <li><a href="listing.php">Law</a></li>
                                            <li><a href="listing.php">Humanities</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children sub-child">
                                        <a href="listing.php">School</a>
                                        <ul>
                                            <li><a href="listing.php">Grade 10</a></li>
                                            <li><a href="listing.php">Grade 9</a></li>
                                            <li><a href="listing.php">Grade 8</a></li>
                                            <li><a href="listing.php">Grade 7</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="contact.php">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="mobile-menu-container"></div>
    </div>
</header>

</div>





