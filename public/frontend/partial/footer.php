<footer>
            <div class="footer-mid section-padding position-relative">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-footer br-2">
                                <div class="footer-title mb-20">
                                    <img src="img/f-logo.png" />
                                </div>
                                <div class="footer-contact">
                                    <p class="adress">
                                        New Baneshwor, Kathamndu, Nepal
                                    </p>
                                    <p><span>Call us now:</span> <a href="tel:+977-98412345678">+977-98412345678</a></p>
                                    <p><span>Email:</span> <a href="mailto:bookstore@gmail.com">bookstore@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-footer br-2">
                                <div class="footer-title mb-20">
                                    <h3>Products</h3>
                                </div>
                                <div class="footer-mid-menu">
                                    <ul>
                                        <li><a href="list.php">Audio Book</a></li>
                                        <li><a href="list.php">E-Books </a></li>
                                        <li><a href="list.php">New products</a></li>
                                        <li><a href="list.php">Best sales</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-footer br-2">
                                <div class="footer-title mb-20">
                                    <h3>Our company</h3>
                                </div>
                                <div class="footer-mid-menu">
                                    <ul>
                                        <li><a href="about.php">About Us</a></li>
                                        <li><a href="blog.php">Blogs</a></li>
                                        <li><a href="faq.php">FAQ</a></li>
                                        <li><a href="contact.php">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-footer">
                                <div class="footer-title mb-20">
                                    <h3>Your account</h3>
                                </div>
                                <div class="footer-mid-menu">
                                    <ul>
                                        <li><a href="login.php">Login</a></li>
                                        <li><a href="register.php">Register </a></li>
                                        <li><a href="checkout.php"> Checkout</a></li>
                                        <li><a href="terms.php">Terms & Condition</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row bt-2">
                        <div class="col-md-12">
                            <div class="copy-right-area text-center">
                                <p>© 2021 <strong> Bookshop.</strong> All Right Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<!-- <div id="back-top">
   <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div> -->

       

<script src="assets/js/jquery.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/jquery-ui/jquery-ui.min.js"></script>
<script src="assets/vendors/masonry/masonry.pkgd.min.js"></script>
<script src="assets/vendors/slick/slick.min.js"></script>
<script src="assets/js/jquery.slicknav.js"></script>
<script src="assets/vendors/owlcarousel/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="assets/js/custom.min.js"></script>

</body>

</html>