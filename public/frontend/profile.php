<?php include 'partial/header.php'; ?>
<section class="book-breadcumb-section">
    <div class="container text-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><i class="fa fa-home"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Profile</li>
            </ol>
        </nav>
    </div>
</section>

<section class="profile section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-wrapper">
                    <div class="user-image">
                        <img src="img/author-img.jpg" alt="" />
                    </div>
                    <div class="user-detail text-center mt-3">
                        <h6>Jeebit Maharjan</h6>
                        <p class="text-secondary">jeebit001@gmail.com</p>
                        <p class="text-secondary">Harisiddhi, Lalitpur</p>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="edit-profile">
                            <h4>Edit Profile</h4>
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Phone Number</label>
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Email Address" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Location</label>
                                        <input type="text" class="form-control" id="location" placeholder="Location" />
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary submit-part">Update Info</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="edit-profile">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Your Ad</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Sold/Expired Ad</a>
                        </li>
                        <li class="nav-item"></li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="active-ad">Active</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                            <button type="button" class="btn btn-danger w-100 mb-2" data-toggle="modal" data-target="#exampleModalCenter">Sold/Delete</button>
                                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Are you sure you want to delete?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">Yes</button>
                                                            <button type="button" class="btn btn-danger">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="active-ad">Active</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                        <button type="button" class="btn btn-danger w-100 mb-2" data-toggle="modal" data-target="#exampleModalCenter">Sold/Delete</button>
                                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Are you sure you want to delete?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">Yes</button>
                                                            <button type="button" class="btn btn-danger">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="active-ad">Active</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                        <button type="button" class="btn btn-danger w-100 mb-2" data-toggle="modal" data-target="#exampleModalCenter">Sold/Delete</button>
                                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Are you sure you want to delete?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">Yes</button>
                                                            <button type="button" class="btn btn-danger">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="active-ad">Active</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                        <button type="button" class="btn btn-danger w-100 mb-2" data-toggle="modal" data-target="#exampleModalCenter">Sold/Delete</button>
                                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Are you sure you want to delete?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">Yes</button>
                                                            <button type="button" class="btn btn-danger">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="sale">Sold</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="product-wrapper">
                                        <div class="product-img">
                                            <a href="#">
                                                <img src="img/product/7.jpg" alt="book" class="primary" />
                                            </a>
                                            <div class="product-flag">
                                                <ul>
                                                    <li><span class="sale">Sold</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-details text-center">
                                            <div class="badge badge-info px-2 py-2">Engineering</div>
                                            <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                            <div class="product-prices">
                                                <ul class="d-flex justify-content-center">
                                                    <li>$30.00</li>
                                                    <li class="old-price">(fixed price)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-footer">
                                            <button type="button" class="btn btn-success w-100">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'partial/footer.php'; ?>
