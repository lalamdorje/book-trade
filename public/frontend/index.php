<?php include 'partial/header.php'; ?>
<main id="content" class="site-main">
    <section class="slider-area" style="background-image: url(img/slider/1.jpg);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="slider-active">
                        <div class="single-slider bg-img">
                            <div class="slider-content">
                                <h2 class="text-white">Trade book</h2>
                                <p class="text-white">Sajha Kitab is an online platform to buy and sell old books. Age of the book doesn't determines the knowledge it contains. So, keep learning keep sharing.
                                    Also, Stay safe.
                                    Please feel free to post your old books so that needy one can get it.</p>
                                <a href="#">Post Ad</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="banner-search-form ">
                        <h3>Search books You Looking For</h3>
                        <form class="form-join">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input class="form-control" autocomplete="off" name="ad_title" placeholder="What Are You Looking For..." type="text" />
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Category</label>
                                <select id="exampleFormControlSelect1" class=" form-control">
                                    <option>Bachelors</option>
                                    <option>Masters</option>
                                    <option>+2</option>
                                    <option>A-level</option>
                                    <option>School</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Location</label>
                                <input class="form-control pac-target-input" name="location" id="sb_user_address" placeholder="Location..." type="text" autocomplete="off" />
                            </div>
                            <div class="search-form-btn mt-4">
                                <button class="main-btn w-100 border-0" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'partial/footer.php'; ?>
