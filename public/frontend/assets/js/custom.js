
$(document).ready( function(){
    $('.search-form-2 i').click(function () {
        $('.search-toggle').toggleClass('toggle');
        $('.search-form-2 i').toggleClass('on');
    });

    $('.featured-slider').owlCarousel({
        loop: true,
        margin: 30,
        navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
        nav: true,
        dots: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $('.testimonials-slider').owlCarousel({
        loop: true,
        margin: 30,
        navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $('.test-slider').owlCarousel({
    loop: true,
    margin: 30,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
    nav: false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});
$('.sub-slider').owlCarousel({
    loop: true,
    margin: 30,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});
$('.sub-slider1').owlCarousel({
    loop: true,
    margin: 30,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});
$('.related-slider').owlCarousel({
    loop: false,
    margin: 30,
    navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa  fa-arrow-right"></i>'],
    nav: false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
});


$(window).on('scroll', function () {
    var scroll = $(window).scrollTop();
    if (scroll < 500) {
        $('#back-top').fadeOut(500);
    } else {
        $('#back-top').fadeIn(500);
    }
    });

    // Scroll Up
    $('#back-top a').on("click", function () {
    $('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
    });

    $(".toggle-accordion").on("click",function(){
        $(".toggle-accordion").toggleClass("active")
        $(".collapse").toggleClass("show")

    })
    $(".header-user-icon").on("click",function(){
        $(".dropdown").toggleClass("show-active")

    })

    /* Category Dropdown Menu  */
    if ($(window).width() < 768) {
        function sidemenuDropdown() {
            var $this = $('.category-menu');
            $this.find('nav.menu .cr-dropdown').find('.left-menu').css('display', 'none');
            $this.find('nav.menu .cr-dropdown ul').slideUp();
            $this.find('nav.menu .cr-dropdown a').on('click', function(e) {
                e.preventDefault();
                $(this).parent('.cr-dropdown').children('ul').slideToggle();
            });
            $this.find('nav.menu .cr-dropdown ul .cr-sub-dropdown ul').css('display', 'none');
            $this.find('nav.menu .cr-dropdown ul .cr-sub-dropdown a').on('click', function(e) {
                e.preventDefault();
                $(this).parent('.cr-sub-dropdown').children('ul').slideToggle();
            });
        }
        sidemenuDropdown();
    }
function getVals() {
    // Get slider values
    let parent = this.parentNode;
    let slides = parent.getElementsByTagName("input");
    let slide1 = parseFloat(slides[0].value);
    let slide2 = parseFloat(slides[1].value);
    // Neither slider will clip the other, so make sure we determine which is larger
    if (slide1 > slide2) {
        let tmp = slide2;
        slide2 = slide1;
        slide1 = tmp;
    }

    let displayElement = parent.getElementsByClassName("rangeValues")[0];
    displayElement.innerHTML = "$" + slide1 + " - $" + slide2;
}

window.onload = function() {
    // Initialize Sliders
    let sliderSections = document.getElementsByClassName("range-slider");
    for (let x = 0; x < sliderSections.length; x++) {
        let sliders = sliderSections[x].getElementsByTagName("input");
        for (let y = 0; y < sliders.length; y++) {
            if (sliders[y].type === "range") {
                sliders[y].oninput = getVals;
                // Manually trigger event first time to display values
                sliders[y].oninput();
            }
        }
    }
}
$('.dropdown-toggle').click(function() { $(this).next('.dropdown').slideToggle();
});

$(document).click(function(e)
{
var target = e.target;
if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle'))
//{ $('.dropdown').hide(); }
  { $('.dropdown').slideUp(); }
});
});


