<?php include 'partial/header.php'; ?>
<section class="book-breadcumb-section">
    <div class="container text-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><i class="fa fa-home"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Post a Ad</li>
            </ol>
        </nav>
    </div>
</section>
<section class="ad section-padding profile">
    <div class="container">
        <div class="ad-form">
            <h4>Post Your Book</h4>
            <form>
            <div class="form-group">
                    <label for="exampleFormControlSelect1">Ad Type</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>Single</option>
                        <option>Bulk</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Title" />
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Categories</label>
                    <select class="form-control" id="exampleFormControlSelect2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect3">Price Type</label>
                    <select class="form-control" id="exampleFormControlSelect3">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="text" class="form-control" id="price" placeholder="Price" />
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect4">Item Condition</label>
                    <select class="form-control" id="exampleFormControlSelect4">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="email">Description</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary submit-part">Upload Now</button>
            </form>
        </div>
    </div>
</section>

<?php include 'partial/footer.php'; ?>
