<?php include('partial/header.php')  ?>
<section class="book-breadcumb-section">
    <div class="container text-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><i class="fa fa-home"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Contact Page</li>
            </ol>
        </nav>
    </div>
</section>
<section class="contact-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="form-contact">
                    <h3>Contact Us</h3>
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fname">Full Name</label>
                                <input type="text" class="form-control" id="fname" placeholder="Full Name" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Phone Number</label>
                                <input type="text" class="form-control" id="inputPassword4" placeholder="Phone Number" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="email" placeholder="Email Address" />
                        </div>
                        <div class="form-group">
                            <label for="email">Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary submit-part">Submit Now</button>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-add">
                    <h3>Get In Touch</h3>
                    <p>We are available to help and advice you with any difficulties.</p>
                    <ul>
                        <li>
                            <div class="media">
                              <i class="fa fa-map-marker"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Address</h5>
                                <p>Devkota Sadak , Mid Baneshowr, Kathmandu 44600, Nepal</p>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <i class="fa fa-phone"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Phone</h5>
                                <p>+977-9841234567, 98412345678</p>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <i class="fa fa-envelope"></i>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">Email</h5>
                                <p>bookstore@gmail.com, info@bookstore.com</p>
                              </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div class="map">
                    <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.7106689216507!2d85.33515341541532!3d27.695335982625814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a44e3c307%3A0x4e54cd2585eaab3c!2sSoftMahal%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sne!2snp!4v1635934410937!5m2!1sne!2snp"
                    width="100%"
                    height="450"
                    style="border: 0;"
                    allowfullscreen=""
                    loading="lazy"
                    ></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('partial/footer.php')  ?>