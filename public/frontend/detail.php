<?php include 'partial/header.php'; ?>
<section class="book-breadcumb-section">
            <div class="container text-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detail Page</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="book-detail-page section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                            <div class="carousel-inner">
                                <div class="carousel-item active"><img src="img/product/1.jpg" alt="book" /></div>
                                <div class="carousel-item"><img src="img/product/10.jpg" alt="book" /></div>
                                <div class="carousel-item"><img src="img/product/11.jpg" alt="book" /></div>
                                <div class="carousel-item"><img src="img/product/13.jpg" alt="book" /></div>
                            </div>
                            <a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
                            <a class="carousel-control-next" href="#custCarousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
                            <ol class="carousel-indicators list-inline">
                                <li class="list-inline-item active">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarousel"> <img src="img/product/1.jpg" class="img-fluid" /> </a>
                                </li>
                                <li class="list-inline-item">
                                    <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarousel"> <img src="img/product/10.jpg" class="img-fluid" /> </a>
                                </li>
                                <li class="list-inline-item">
                                    <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarousel"> <img src="img/product/11.jpg" class="img-fluid" /> </a>
                                </li>
                                <li class="list-inline-item">
                                    <a id="carousel-selector-2" data-slide-to="3" data-target="#custCarousel"> <img src="img/product/13.jpg" class="img-fluid" /> </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="detail-desc">
                            <div class="head-rate-desc">
                                <h4>Where the Crawdads Sing</h4>
                                <h6><span>By:</span>Jeebit Maharjan</h6>
                                <h6><span>Posted on:</span>Nov 26, 2022</h6>
                                <div class="rate-price">
                                    <div class="price-part">
                                        <p>Rs 250 (fixed) </p>
                                    </div>
                                </div>
                                <div class="book-desc">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                                        qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                            <div class="detail-btn mt-3">
                                <button class="btn main-btn"><i class="fas fa-comments mr-2"></i>Send Message</button>
                                <button class="btn main-btn ph-btn ml-3"><i class="fas fa-mobile-alt mr-2"></i>9849364658</button>
                            </div>
                            <div class="share mt-4">
                                <span>Share This:</span>
                                <ul>
                                    <li>
                                        <a href="#" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="instagram" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="book-description-tab">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Product Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">Reviews (0)</a>
                                </li>
                            </ul>
                            <div class="tab-content book-tab" id="myTabContent">
                                <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                    <p>
                                        Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko
                                        farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna
                                        delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry
                                        richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                                    <table class="table table-hover table-borderless text-center mt-3">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Format:</th>
                                                <td>Paperback | 384 pages</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Dimensions:</th>
                                                <td>9126 x 194 x 28mm | 301g</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Publication date:</th>
                                                <td>20 Dec 2020</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Publisher:</th>
                                                <td>Little, Brown Book Group</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Imprint:</th>
                                                <td>Corsair</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Publication City/Country:</th>
                                                <td>London, United Kingdom</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Language:</th>
                                                <td>English</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <div class="cust-rev">
                                                <h5>Customer Review</h5>
                                            </div>
                                            <div class="customer-review d-flex align-items-center">
                                                <span class="rate">4.6</span>
                                                <div class="ml-3 h6 mb-0">
                                                    <span class="rev">3,714 reviews</span>
                                                    <div class="text-yellow-darker">
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex btn-review">
                                                <button>See All Reviews</button>
                                                <button>Write A Review</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="star-rating-list">
                                                <ul>
                                                    <li>
                                                        <div class="row align-items-center">
                                                            <div class="col-md-2"><span class="star">5 Stars</span></div>
                                                            <div class="col-md-8">
                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 70%;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="num">50</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row align-items-center">
                                                            <div class="col-md-2"><span class="star">4 Stars</span></div>
                                                            <div class="col-md-8">
                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 40%;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="num">50</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row align-items-center">
                                                            <div class="col-md-2"><span class="star">3 Stars</span></div>
                                                            <div class="col-md-8">
                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 30%;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="num">50</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row align-items-center">
                                                            <div class="col-md-2"><span class="star">2 Stars</span></div>
                                                            <div class="col-md-8">
                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 50%;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="num">50</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row align-items-center">
                                                            <div class="col-md-2"><span class="star">1 Stars</span></div>
                                                            <div class="col-md-8">
                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 10%;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="num">50</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="review-list">
                                                <span>1-5 of 44 reviews</span>
                                                <ul>
                                                    <li>
                                                        <div class="d-flex justify-content-between mt-4">
                                                       <h3>Get the best seller at a great price.</h3>
                                                       <div class="text-yellow-darker">
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                    </div>
                                                       </div> 
                                                       <h6>February 22, 2020</h6>
                                                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

                                                    </li>
                                                    <li>
                                                        <div class="d-flex justify-content-between mt-4">
                                                       <h3>Get the best seller at a great price.</h3>
                                                       <div class="text-yellow-darker">
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                        <small class="fa fa-star"></small>
                                                    </div>
                                                       </div> 
                                                       <h6>February 22, 2020</h6>
                                                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="related-books section-padding">
            <div class="container">
                <h3 class="text-center mb-4">Realated Products</h3>
                <div class="col-md-12">
                    <div class="related-slider owl-carousel owl-theme ">
                    <div class="product-wrapper">
                            <div class="product-img">
                                <a href="#">
                                    <img src="img/product/7.jpg" alt="book" class="primary" />
                                </a>
                                <div class="product-flag">
                                    <ul>
                                        <li><span class="sale">new</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-details text-center">
                                <div class="badge badge-info px-2 py-2">Engineering</div>
                                <h4><a href="detail.php">Strive Shoulder Pack</a></h4>
                                <div class="product-price">
                                    <ul>
                                        <li>$30.00</li>
                                        <li class="old-price">(fixed price)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-link text-center">
                                <div class="product-button">
                                    <a href="detail.php" title="Add to cart">View Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


<?php include 'partial/footer.php'; ?>
